<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('path');
        $this->load->model('admin_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->load->library('pagination');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index()
    {
    	$this->load->view('login_layout/header');
        $this->load->view('login');
        $this->load->view('login_layout/footer');
    }



    function checkLogin()
    {

        $check = $this->login_model->checkDeactivate();
        if ($check > 0) {
            $this->session->set_flashdata('errors', '<p style="color: #CC0000;">Your Account has been deactivated</p>');
            redirect('cRJDJK845-DFdx3');
        } else {
            $result = $this->login_model->validate();
            if ($result) {
                redirect('admin/dashboard');
            } else {
                $this->session->set_flashdata('errors', '<p style="color: #CC0000;">Incorrect username or password</p>');
                redirect('cRJDJK845-DFdx3');
            }

        }

    }

    function activateAdmin()
    {
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] == '1') {
            $data = $this->login_model->activateAdmin();
            if ($data) {
                $this->session->set_flashdata('activate_user', 'done');
            } else {
                $this->session->set_flashdata('activate_user', 'error');
            }
            redirect('admin/all-users/1');
        } else {
            redirect('cRJDJK845-DFdx3');
        }
    }

    function deactivateAdmin()
    {
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] == '1') {
            $data = $this->login_model->deactivateAdmin();
            if ($data) {
                $this->session->set_flashdata('deactivate_user', 'done');
            } else {
                $this->session->set_flashdata('deactivate_user', 'error');
            }
            redirect('admin/all-users/1');
        } else {
            redirect('cRJDJK845-DFdx3');
        }
    }

    function dashboard(){
        if ($this->checkIsValidated()) {
            $monday = date( 'Y-m-d', strtotime( 'monday this week' ) );
            $friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
            $datetoday = date('Y-m-d');
            $data['total'] = $this->admin_model->getAllPassenger();
            $data['total_rider'] = $this->admin_model->getAllRider();
            $queryETW = "select count(tripID) as totalRE, ifnull(sum(totalAmount),0) as totalEarning from ride_history where rideDate like '$datetoday%'";
            $getREdetails = $this->admin_model->getDetailQuery($queryETW);
            $queryRatings = "select ifnull(sum(urRate)/count(uID),0) as totalratings from user_review";
            $getRatings = $this->admin_model->getDetailQuery($queryRatings);
            $data['getRideThisWeek'] =  $getREdetails[0]['totalRE'];
            $data['getEarningThisWeek'] =  $getREdetails[0]['totalEarning'];
            $data['getTotalRatings'] = $getRatings[0]['totalratings'];
            $this->load->view('common/header', $data);
            $this->load->view('common/footer');
            $this->load->view('dashboard');
            
        } else {
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function getRiderHistory(){

        $rideWeekStat = $this->input->get('rideWeekStat');
        if($rideWeekStat == 'Today' || empty($rideWeekStat) ){
            $datetoday = date('Y-m-d');
            $where = "where rideDate like '$datetoday%'";
        }
        if($rideWeekStat == 'Week'){
            $monday = date( 'Y-m-d', strtotime( 'monday this week' ) );
            $friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
            $where = "where date_format(rideDate,'%Y-%m-%d') between '$monday' and '$friday'";
        }
        if($rideWeekStat == 'Month'){
            $Ymonth = date( 'Y-m');
            $where = "where date_format(rideDate,'%Y-%m') = '$Ymonth'";
        }
      
        $data_points = array();
        $data = $this->admin_model->getRiderHistory($where);
  		if(empty($data)){
  			$point = array('y' => date('h A') , 'a' => 0);
  			array_push($data_points, $point);
  		}
        foreach ($data as $row) {
            $point = array('y' => $row['rhourDay'] , 'a' => $row['totalTrip']);
            array_push($data_points, $point);
        }
        echo json_encode($data_points, JSON_NUMERIC_CHECK);
    }
    public function getRiderEarnings(){
        $REweekStat = $this->input->post('REweekStat');
        if($REweekStat == 'Today' || empty($REweekStat) ){
            $datetoday = date('Y-m-d');
            $where = "where date_format(rideDate, '%Y-%m-%d') = '$datetoday'";
        }
        if($REweekStat == 'Week'){
            $monday = date( 'Y-m-d', strtotime( 'monday this week' ) );
            $friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
            $where = "where date_format(rideDate,'%Y-%m-%d') between '$monday' and '$friday'";
        }
        if($REweekStat == 'Month'){
            $Ymonth = date( 'Y-m');
            $where = "where date_format(rideDate,'%Y-%m') = '$Ymonth'";
        }
        $queryETW = "select count(tripID) as totalRE, ifnull(sum(totalAmount),0) as totalEarning
                    from ride_history $where";
        $getREdetails = $this->admin_model->getDetailQuery($queryETW);
        echo json_encode($getREdetails);
    }

    public function getEarningPerMonth(){
        $year = date('Y');
        $earnMonthStat = $this->input->get('earnMonthStat');
        if(empty($earnMonthStat) || $earnMonthStat == $year){
            $where = date('Y');
            $where = "where date_format(rideDate, '%Y') = '$where'";
        }else{
            $datewhere = (date('Y-').$earnMonthStat);
            $where = date('Y-m', strtotime($datewhere));
            $where = "where date_format(rideDate, '%Y-%m') = '$where'";
        }
        $query = "SELECT sum(totalAmount) as totaEarning,
                    date_format(rideDate, '%Y-%m') as rYmonth,
                    date_format(rideDate, '%Y') as rYear
                    from ride_history
                    $where
                    group by rYmonth";

        $data = $this->admin_model->getDetailQuery($query);
        $data_points = array();
        $point1 = array('m' => '0', 'a' => '0');
        array_push($data_points, $point1);
        foreach ($data as $row) {
            $point = array('m' => $row['rYmonth'] , 'a' => $row['totaEarning'], 'y' => $row['rYear']);
            array_push($data_points, $point);
        }
        echo json_encode($data_points,JSON_NUMERIC_CHECK);
      
    }
    public function checkIsValidated()
    {
        if ($this->session->userdata('validated')) {
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('cRJDJK845-DFdx3', 'refresh');
    }

    function changePassword() {
        $this->load->view('common/header');
        $this->load->view('change-password');
        $this->load->view('common/footer');
    }

    function changePasswordAction() {
        $this->form_validation->set_rules('new_password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('change-password');
        } else {
            $this->login_model->changePasswordAction($this->session->userdata['admin_user']);
            redirect('change-password');
        }
    }

    function checkPassword() {
        $pass = $this->security->xss_clean($this->input->post('password'));
        $sql = $this->login_model->checkPass($pass, $this->session->userdata['admin_user']);
        echo json_encode(array("validation"=> $sql));
    }

    function createUser() {
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1') {
            $this->load->view('common/header');
            $this->load->view('create-user');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function addUser() {
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1') {
            $check_email = $this->login_model->checkEmail();
            if(count($check_email) > 0) {
                $this->session->set_flashdata('add_user', 'email_exist');
            }else{
                $data = $this->login_model->addUser();
                if($data) {
                    $this->session->set_flashdata('add_user', 'done');
                } else {
                    $this->session->set_flashdata('add_user', 'error');
                }
            }
            redirect('admin/create-user');

        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function editUser(){
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1') {
            $user = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['user'] = $this->login_model->userData($user);
            $this->load->view('common/header');
            $this->load->view('edit-user',$data);
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function editUserInfo(){
        $user =$this->input->post('user');
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1') {
            $check_email = $this->login_model->checkEmailUser($user);
            if(count($check_email) > 0) {
                $this->session->set_flashdata('edit_user', 'email_exist');
            }else{
                $data = $this->login_model->editUserInfo();
                if($data) {
                    $this->session->set_flashdata('edit_user', 'done');
                } else {
                    $this->session->set_flashdata('edit_user', 'error');
                }
            }
            redirect('admin/edit-user/'.$user);

        }else{
            redirect('cRJDJK845-DFdx3');
        }

    }
    function sortUser(){
        if ($this->checkIsValidated() && ($this->session->userdata['rider_type'] =='1' || $this->session->userdata['rider_type'] =='2') )   {
            $config = array();
            $option = $this->admin_model->joinDate($this->input->post('option'));
            $config['base_url'] = base_url() . 'admin/sort-user';
            $config['total_rows'] = $this->admin_model->sortNormalUserCount($option);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->sortNormalUserCount($option);
            $data['date'] = $option;
            $data['status'] = "All";
            $data['all_riders'] = $this->admin_model->sortAllNormalUser($page, $config['per_page'],$option);
            $this->load->view('common/header', $data);
            $this->load->view('all-riders');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function allUser(){

        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
            $config = array();
            $config['base_url'] = base_url() . 'admin/all-bikes';
            $config['total_rows'] = $this->admin_model->userCount();
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->userCount();
            $data['all_users'] = $this->admin_model->getAllUser($page, $config['per_page']);
            $datas = $this->admin_model->getAllUser($page, $config['per_page']);
            // $this->debug($config,1);
            $this->load->view('common/header', $data);
            $this->load->view('all-bikes');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
    function viewUser(){
        if ($this->checkIsValidated() && ($this->session->userdata['user_type'] =='1' || $this->session->userdata['user_type'] =='2') )   {
            $option = $this->admin_model->userStatus($this->input->post('status'));
            $config = array();
            $config['base_url'] = base_url() . 'admin/view-user/';
            $config['total_rows'] = $this->admin_model->normalUserCount($option);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->normalUserCount($option);
            $data['date'] = null;
            $data['status'] = $option;
            $data['all_riders'] = $this->admin_model->getAllNormalUser($page, $config['per_page'],$option);
            $this->load->view('common/header', $data);
            $this->load->view('all_riders');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }




    function allRider($g=null,$tab= null){
    	$btn_search_date = $this->input->post('btn_search_date');
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
                $fromdate = $this->input->post('startDate1');
                $enddate = $this->input->post('endDate1');

        	    $config = array();
	            $config['base_url'] = base_url() . 'admin/all-riders';
	            $config['total_rows'] = $this->admin_model->userCount();
	            $config['per_page'] = 50;
	            $choice = $config["total_rows"] / $config["per_page"];
	            $config["num_links"] = round($choice);
	            $config['full_tag_open'] = '<ul class="pagination">';
	            $config['full_tag_close'] = '</ul>';
	            $config['first_link'] = false;
	            $config['last_link'] = false;
	            $config['first_tag_open'] = '<li class="paginate_button">';
	            $config['first_tag_close'] = '</li>';
	            $config['next_link'] = 'NEXT';
	            $config['next_tag_open'] = '<li class="paginate_button next">';
	            $config['next_tag_close'] = '</li>';
	            $config['prev_link'] = 'PREVIOUS';
	            $config['prev_tag_open'] = '<li class="paginate_button previous">';
	            $config['prev_tag_close'] = '</li>';
	            $config['last_tag_open'] = '<li class="paginate_button">';
	            $config['last_tag_close'] = '</li>';
	            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
	            $config['cur_tag_close'] = '</a></li>';
	            $config['num_tag_open'] = '<li class="paginate_button">';
	            $config['num_tag_close'] = '</li>';
        	if($tab=='pending'){
	            $this->pagination->initialize($config);
	            $data['pagination_links'] = $this->pagination->create_links();
	            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	            $data['total_db_rows'] = $this->admin_model->userCount();
	            
	            $rdocID = $this->admin_model->getAllRiderStatus3();
                $whereDate ='';
                if(isset($btn_search_date)){
	               $whereDate = "date_format(rJoinDate, '%Y-%m-%d') between '$fromdate' and '$enddate'";
                }
	            $data['all_riders'] = $this->admin_model->getAllRidersPending($page, $config['per_page'], $rdocID, $whereDate);
	            $data['tab_name'] = $tab;
                $data['fromdate'] = $fromdate;
                $data['enddate'] = $enddate;
        	}elseif($tab == 'payments'){
                
        		$this->pagination->initialize($config);
	            $data['pagination_links'] = $this->pagination->create_links();
	            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	            $data['total_db_rows'] = $this->admin_model->userCount();
                $where = '';
                $datesearchcat='';
                if(isset($btn_search_date)){
                    $where = "date_format(u.uJoinDate, '%Y-%m-%d') between '$fromdate' and '$enddate'";
                }
                $datesearch = $this->input->post('datesearch');
                if(isset($datesearch)){
                    $where = '';
                    $date_today = date('Y-m-d');
                    $this_week_from = date( 'Y-m-d', strtotime( 'monday this week' ) );
                    $this_week_to = date( 'Y-m-d', strtotime( 'friday this week' ) );
                    $this_month = date('Y-m');
                    if($datesearch == 'Today'){
                        $where = "date_format(u.uJoinDate, '%Y-%m-%d') = '$date_today'";
                        $datesearchcat = 'Today';
                    }
                    if($datesearch == 'This Week'){
                        $where = "date_format(u.uJoinDate, '%Y-%m-%d') between '$this_week_from' and '$this_week_to'";
                        $datesearchcat = 'This Week';
                    }
                    if($datesearch == 'This Month'){
                        $where = "date_format(u.uJoinDate, '%Y-%m-%d') = '$this_month'";
                        $datesearchcat = 'This Month';
                    }
                }
	            $data['get_payments'] = $this->admin_model->getPayments($page, $config['per_page'], $where);
                // $this->debug($data['get_payments'], 1);
        		$data['tab_name'] = $tab;
                $data['fromdate'] = $fromdate;
                $data['enddate'] = $enddate;
                $data['datesearchcat'] = $datesearchcat;
        	}elseif($tab == 'ride_history'){
        		$this->pagination->initialize($config);
	            $data['pagination_links'] = $this->pagination->create_links();
	            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	            $data['total_db_rows'] = $this->admin_model->userCount();
                $whereDate = '';
                if(isset($btn_search_date)){
                    $whereDate = "date_format(u.uJoinDate, '%Y-%m-%d') between '$fromdate' and '$enddate'";
                }
	            $data['ride_history'] = $this->admin_model->getRide_history($page, $config['per_page'], $whereDate);
        		$data['tab_name'] = $tab;
                $data['fromdate'] = $fromdate;
                $data['enddate'] = $enddate;
        	}else{
	            $this->pagination->initialize($config);
	            $data['pagination_links'] = $this->pagination->create_links();
	            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	            $data['total_db_rows'] = $this->admin_model->userCount();
                $where = '';
                if(isset($btn_search_date)){
                    $where = "date_format(rJoinDate, '%Y-%m-%d') between '$fromdate' and '$enddate'";
                }
                $data['fromdate'] = $fromdate;
                $data['enddate'] = $enddate;
	            $data['all_riders'] = $this->admin_model->getAllRiders($page, $config['per_page'], $where);
	            $data['tab_name'] = $tab;
        	}
            	$this->load->view('common/header', $data);
	            $this->load->view('all-riders');
	            $this->load->view('common/footer');	
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function sortDateJoin(){
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
            $fromdate = $this->input->post('startDate1') . ' 00:00:00';
            $enddate = $this->input->post('endDate1') . ' 23:59:59';

            $config = array();
            $config['base_url'] = base_url() . 'admin/all-riders';
            $config['total_rows'] = $this->admin_model->userCount();
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->userCount();
            $where = "rJoinDate between '$fromdate' and '$enddate'";
            $data['all_riders'] = $this->admin_model->getAllRiders($page, $config['per_page'], $where);
            $data['tab_name'] = $tab;

            $this->load->view('common/header', $data);
            $this->load->view('all-riders');
            $this->load->view('common/footer'); 
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    function addRiderInfo(){
        $location = $this->admin_model->addRider();
        $this->admin_model->addDocumentImg($location);

        redirect('admin/all-riders');
    }
    function checkFacility($rID,$faID){
        $facility = $this->admin_model->checkFacility($rID,$faID);
        return $facility;
    }

    function searchUser(){
        if ($this->checkIsValidated() && ($this->session->userdata['user_type'] =='1' || $this->session->userdata['user_type'] =='2') )   {
            $config = array();
            $option = $this->admin_model->searchUser($this->input->post('search'));
            $config['base_url'] = base_url() . 'admin/sort-user';
            $config['total_rows'] = $this->admin_model->searchUserCount($option);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->searchUserCount($option);
            $data['date'] = '';
            $data['status'] = "All";
            $data['search'] = $option;
            $data['all_users'] = $this->admin_model->searchAllNormalUser($page, $config['per_page'],$option);
            $this->load->view('common/header', $data);
            $this->load->view('rider');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }



    function searchBike(){
        if ($this->checkIsValidated()  )   {
            $config = array();
            $option = $this->admin_model->searchBike($this->input->post('search'));
            $config['base_url'] = base_url() . 'admin/sort-user';
            $config['total_rows'] = $this->admin_model->searchBikeCount($option);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->searchBikeCount($option);
            $data['date'] = '';
            $data['status'] = "All";
            $data['search'] = $option;
            $data['all_bikes'] = $this->admin_model->searchAllNormalBikes($page, $config['per_page'],$option);
            $this->load->view('common/header', $data);
            $this->load->view('bikes');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    function searchBikeAndRider(){
        if ($this->checkIsValidated()  )   {
            $option = "";
            $total_rows = "";
            $total_db_rows = "";
            $per_page = "";
            $page = "";
            $search_category = $this->input->post('search_category');
            $config = array();
            if($search_category == 'bikes'){
                $option = $this->admin_model->searchBike($this->input->post('search'));
                $total_rows = $this->admin_model->searchBikeCount($option);
                $total_db_rows =$this->admin_model->searchBikeCount($option);
                $per_page = 50;
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_bikes'] = $this->admin_model->searchAllNormalBikes($page, $per_page,$option);
            }
            if($search_category == 'riders'){
                $option = $this->admin_model->searchUser($this->input->post('search'));
                $total_rows = $this->admin_model->searchUserCount($option);
                $total_db_rows =$this->admin_model->searchUserCount($option);
                $per_page = 50;
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['all_users'] = $this->admin_model->searchAllNormalUser($page, $per_page,$option);
            }
            if($search_category == 'passenger'){
                $option = $this->admin_model->searchUser($this->input->post('search'));
                $total_rows = $this->admin_model->searchUserCount($option);
                $total_db_rows =$this->admin_model->searchUserCount($option);
                $per_page = 50;
                $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $data['list_passenger'] = $this->admin_model->searchAllPassenger($page, $per_page,$option);
                $data['fromdate'] = date('Y-m-d');
                $data['enddate'] =date('Y-m-d');
            }
            $config['base_url'] = base_url() . 'admin/sort-user';
            $config['total_rows'] = $total_rows;
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $total_db_rows;
            $data['date'] = '';
            $data['status'] = "All";
            $data['search'] = $option;
            

            if($search_category == 'bikes'){
                $this->load->view('common/header', $data);
                $this->load->view('bikes');
                $this->load->view('common/footer');
            }elseif($search_category == 'riders'){
                $this->load->view('common/header', $data);
                $this->load->view('rider');
                $this->load->view('common/footer');
            }elseif($search_category == 'passenger'){
                $this->load->view('common/header', $data);
                $this->load->view('common/footer');
                $this->load->view('all-passenger');
            }else{
                redirect('admin/dashboard');
            }
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    // sort location by added date
    function sortLocation(){
        if ($this->checkIsValidated()) {
            $option = 'All';
            $from = $this->admin_model->sortFrom($this->input->post('from'));
            $to = $this->admin_model->sortTo($this->input->post('to'));
            $config = array();
            $config['base_url'] = base_url() . 'admin/sort-all-locations/';
            $config['total_rows'] = $this->admin_model->getSortLocationCount($from,$to);
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->getSortLocationCount($from,$to);
            $data['park'] = $option;
            $data['from'] = $from;
            $data['to'] = $to;
            $data['type'] = '';
            $data['all_location'] = $this->admin_model->getSortLocation($page, $config['per_page'],$from,$to);

            $this->load->view('common/header', $data);
            $this->load->view('all-location');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    function editRider(){
        if ($this->checkIsValidated()) {
            $location = $this->uri->segment(3);
            // $data['location'] = $this->admin_model->getLocationDetails($location);
            $data['location'] = $this->admin_model->GetInfoByRow('rider','rID', array('rID' => $location));
            $data['location_img'] = $this->admin_model->getDocumentImages($location);
            $data['internal_bikes'] = $this->admin_model->getInternalBike();
            $data['price_master_details'] = $this->admin_model->GetAllInfo('price_master_details','pdID','');
            $this->load->view('common/header', $data);
            $this->load->view('common/footer');
            $this->load->view('edit-rider');            
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    function pendingRider(){
 
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
            $config = array();
            $config['base_url'] = base_url() . 'admin/pending-rider';
            $config['total_rows'] = $this->admin_model->userCount();
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->userCount();
            
            $rdocID = $this->admin_model->getAllRiderStatus3();
            // $wheres = array($rdocID => $rdocID);
            $data['all_riders'] = $this->admin_model->getAllRidersPending($page, $config['per_page'], $rdocID);
            $this->load->view('common/header', $data);
            $this->load->view('common/footer');
            $this->load->view('all-pending-rider');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function ActionpendingRider(){
        if ($this->checkIsValidated()) {
            $location = $this->uri->segment(3);
            // $data['location'] = $this->admin_model->getLocationDetails($location);
            $data['location'] = $this->admin_model->GetInfoByRow('rider','rID', array('rID' => $location));
            $data['location_img'] = $this->admin_model->getDocumentImages($location);
            $data['internal_bikes'] = $this->admin_model->getInternalBike();
            $data['price_master_details'] = $this->admin_model->GetAllInfo('price_master_details','pdID','');
            $this->load->view('common/header', $data);
            $this->load->view('common/footer');
            $this->load->view('pending-rider');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function delete()
    {

        $id = $this->uri->segment(3);

        if (empty($id)) {
            show_404();
        }

        $news_item = $this->admin_model->get_news_by_id($id);

        if ($news_item['user_id'] != $this->session->userdata('user_id')) {
            $currentClass = $this->router->fetch_class(); // class = controller
            redirect(site_url($currentClass));
        }

        $this->admin_model->delete_news($id);
        redirect( base_url() . 'index.php/news');
    }

    public function delete_book()
    {
        //get data from myview
        $bikeId = $this->input->post('bID');
        $this->load->model('Admin_model');
        $this->admin_model->book_delete($bikeId);
        echo "Book Deleted Successfully!";
    }

    function viewBike(){

        if ($this->checkIsValidated()) {
            $location = $this->uri->segment(3);
            $data['location'] = $this->admin_model->getBikeDetails($location);
            $data['location_img'] = $this->admin_model->getDocumentImages($location);
            $this->load->view('common/header', $data);
            $this->load->view('view-bike');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }

    function addRider(){
        $data['internal_bikes'] = $this->admin_model->getInternalBike();
        $data['price_master_details'] = $this->admin_model->GetAllInfo('price_master_details','pdID','');
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('add-rider',$data);
    }
    function getVehicleNo(){
        $vn = $this->admin_model->getDetailQuery('select rAssignBike from rider group by rAssignBike');
        $vno ='';
        foreach ($vn as $v) {
            if(empty($v['rAssignBike'])) continue;
            $vno .= '"'.$v['rAssignBike'].'",';
        }
        $vnof = rtrim($vno, ',');
        $vt = $this->input->post('vehicle_type');
        // $vt = 'Tuk';
        $data=$this->admin_model->getVehicleNo($vt, $vnof );
        echo json_encode($data);
    }
    function addBikes(){
    $data['internal_riders'] = $this->admin_model->getInternalRider();
    $data['price_master_details'] = $this->admin_model->GetAllInfo('price_master_details','pdID','');
    $this->load->view('common/header');
    $this->load->view('add-bikes',$data);
    $this->load->view('common/footer');

    }
    function editBikes($id=null){
    $location = $this->uri->segment(3);
    $data['location'] = $this->admin_model->getLocationDetails($location);
    $data['location_img'] = $this->admin_model->getDocumentImages($location);
    $data['internal_riders'] = $this->admin_model->getInternalRider();
    $data['price_master_details'] = $this->admin_model->GetAllInfo('price_master_details','pdID','');
    $data['price_master_details'] = $this->admin_model->GetAllInfo('price_master_details','pdID','');
    $data['edit_bike_list'] = $this->admin_model->GetInfoByRow('bikes', 'bID', array('bID' => $id));
    $this->load->view('common/header');
    $this->load->view('edit-bikes',$data);
    $this->load->view('common/footer');

    }
    function editBikesProcess(){
       
        $form_data = array(
                'bBrand' => $this->input->post("brand"),
                'vehicle_cat' => $this->input->post("vehicles"),
                'bType' => $this->input->post("type"),
                'bChassisNumber' => $this->input->post('chassis_number'),
                'bVehicleNumber' => $this->input->post('vehicle_number'),
                'bAssignRider' => $this->input->post('internal-rider'),
                'bInsuranceExpiry' => $this->input->post('insurance_exp')
        );
        $id = $this->input->post('hddbID');
        $this->admin_model->Manage('bikes',$form_data, array('bID' => $id),'update');
        redirect('admin/all-bikes/1',$data);
    }
    function addLocation1(){
        $data['bikes'] = $this->admin_model->addBikes();

        redirect('admin/all-bikes/1',$data);
    }

    function addRider1(){
        $document = $this->admin_model->addRider();
        $this->admin_model->addDocumentImg($document);
        redirect('admin/all-riders/1');
    }
    function editRiderProcess(){
        $equipments = $this->input->post('equipments');
        $equip = '';
        foreach ($equipments as $eq) {
            $equip .= $eq.',';
        }
        $form_data = array(
            'rName' => $this->input->post('rider_name'),
            'rPhone' => $this->input->post('contact_no'),
            'rEmail' => $this->input->post('rider_email'),
            'rEpfNumber' => $this->input->post('rider_epfno'),
            'rSalary' => $this->input->post('rider_salary'),
            'rEmiNumber' => $this->input->post('phone_emi'),
            'rEquipments' => rtrim($equip, ','),
            'rInitialDeposite' => $this->input->post('initial_deposit'),
            'rBykerPhoneNumber' => $this->input->post('byker_phone_number'),
            'rLastName' => $this->input->post('last_name'),
            'rInExRiders' => $this->input->post('rInExRiders')
            // 'driving_expe=' => $this->input->post('driving_expe')
        );
        $where = array('rID' => $this->input->post("hddrID"));
        $test = $this->admin_model->Manage('rider',$form_data, $where, 'update');
        $this->admin_model->RDelete('rider_document', $where);
        $this->admin_model->addDocumentImg($this->input->post("hddrID"));
       redirect('admin/all-riders/1');
    }
    public function approve_reject_process(){
        
        $action = $this->input->post('btn_submit');
        if($action == 'Accept'){
            $rdStatus = 3;
        }else{
            $rdStatus = 0;
        }
        $form_data = array(
            'rdStatus' => $rdStatus,
            'remarks' => $this->input->post('remarks')
        );
        $hddrdID = $this->input->post('hddrdID');
        $where = array('rdID' => $hddrdID);
        $this->admin_model->Manage('rider_document',$form_data, $where, 'update');
        $hddrID = $this->input->post('hddrID'); // location
        redirect("admin/action-pending-rider/$hddrID");
    }
    public function promo_codes($action=null){
        switch (strtoupper($action)) {
            case 'ADD':
                $data['title'] = 'Promo Codes';
                $this->load->view('common/header');
                $this->load->view('common/footer');
                $this->load->view('add-promo-codes',$data);
            break;
            
            default:
               
                $pcQuery = "select id as idpc
                            from promo_code where date_format(expiration,'%Y-%m-%d') < date_format(now(),'%Y-%m-%d')
                            ";
                $idPClist = $this->admin_model->getDetailQuery($pcQuery);
                $form_data_pm = array(
                        'status' => 3 // status 3 = expired
                    );
                $where = array();
                foreach ($idPClist as $idp) {
                    $where = array('id' => $idp['idpc']);
                    $this->admin_model->Manage('promo_code',$form_data_pm, $where, 'update');
                }
                $data['promo_list'] = $this->admin_model->GetAllInfo('promo_code','id',array());
                $this->load->view('common/header');
                $this->load->view('common/footer');
                $this->load->view('promo-codes',$data);
                
            break;
        }   
    }
    public function addPromocode(){
        $btn_save = $this->input->post('btn_save');
        if($btn_save == 'Done'){
            $status = 1;
        }
        if($btn_save == 'Discard'){
            $status = 2;
        }
        $passenger = $this->input->post('all_passenger');
            $form_data = array(
                'promoCode' => $this->input->post('promo_code'),
                'startDate' => $this->input->post('start_date'),
                'expiration' => $this->input->post('expiration'),
                'discountAmount' => $this->input->post('discount'),
                'discountType' => $this->input->post('discount_type'),
                'fromDate' => $this->input->post('startDate1'),
                'toDate' => $this->input->post('endDate1'),
                'validNumberOfRides' => $this->input->post('validNoRiders'),
                'message' => $this->input->post('message'),
                'filterType' => $this->input->post('recipients'),
                'ride_total' => $this->input->post('ride_total'),
                'all_passenger' => ($passenger) ? 1 : 0,
                'status' => $status
            );
        $this->admin_model->SaveForm('promo_code',$form_data);
        redirect("admin/promo_codes");
    }
    public function draftPromo(){
        $idpc= $this->input->post('id');
        $where = array('idpc' => $idpc);
        $this->admin_model->RDelete('promo_code',$where);
        redirect("admin/promo_codes");
    }
    function passenger(){
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
            $fromdate = $this->input->post('startDate1');
            $enddate = $this->input->post('endDate1');
            $btn_search_date = $this->input->post('btn_search_date');

            $config = array();
            $config['base_url'] = base_url() . 'admin/pending-rider';
            $config['total_rows'] = $this->admin_model->userCount();
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['total_db_rows'] = $this->admin_model->userCount();
            $data['fromdate'] = $fromdate;
            $data['enddate'] = $enddate;
            // $rdocID = $this->admin_model->getAllRiderStatus3();
            // $wheres = array($rdocID => $rdocID);
            $whereDate = '';
            if(isset($btn_search_date)){
                   $whereDate = "date_format(u.uJoinDate, '%Y-%m-%d') between '$fromdate' and '$enddate'";
                }
            $data['list_passenger'] = $this->admin_model->getListPassenger($page, $config['per_page'], $whereDate);
            $this->load->view('common/header', $data);
            $this->load->view('common/footer');
            $this->load->view('all-passenger');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function viewPassenger($uid = null, $tab=null){
        if ($this->checkIsValidated() && $this->session->userdata['user_type'] =='1')  {
            $config = array();
            $config['base_url'] = base_url() . 'admin/view-passenger';
            $config['total_rows'] = $this->admin_model->userCount();
            $config['per_page'] = 50;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['first_link'] = false;
            $config['last_link'] = false;
            $config['first_tag_open'] = '<li class="paginate_button">';
            $config['first_tag_close'] = '</li>';
            $config['next_link'] = 'NEXT';
            $config['next_tag_open'] = '<li class="paginate_button next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = 'PREVIOUS';
            $config['prev_tag_open'] = '<li class="paginate_button previous">';
            $config['prev_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="paginate_button">';
            $config['last_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="paginate_button">';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $data['pagination_links'] = $this->pagination->create_links();
            $page = 0;
            $data['total_db_rows'] = $this->admin_model->userCount();
            if($tab=='planned_rides'){
                $uid = $this->uri->segment(3);
                $where = array('sr.uID' => $uid);
                $data['get_details_passenger'] = $this->admin_model->GetInfoByRow('user','uID',array('uID' => $uid));
                $data['ride_total'] = $this->admin_model->get_planned_history_view($page, $config['per_page'], $where, '');
                $data['list_passenger'] = $this->admin_model->get_planned_history_view($page, $config['per_page'], $where, 'r.bID, sr.uID');
                $data['tab_name'] = 'planned_rides';
                $data['uID'] = $uid;
            }else{
                $uid = $this->uri->segment(3);
                $where = array('rh.uID' => $uid);
                $data['get_details_passenger'] = $this->admin_model->GetInfoByRow('user','uID',array('uID' => $uid));
                $data['ride_total'] = $this->admin_model->get_ride_history_view($page, $config['per_page'], $where, '');
                $data['list_passenger'] = $this->admin_model->get_ride_history_view($page, $config['per_page'], $where, 'r.bID, rh.uID');
                $data['tab_name'] = '';
                $data['uID'] = $uid;
            }
            
            $this->load->view('common/header', $data);
            $this->load->view('view-passenger');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function setting($tab=null){
        if ($this->checkIsValidated()) {
            if($tab == 'set-password' || empty($tab)){
                $data['total_db_rows']='';
                $data['tab_name'] = $tab;
            }
            if($tab == 'set-roles'){
                $config = array();
                $config['base_url'] = base_url() . 'admin/setting';
                $config['total_rows'] = $this->admin_model->userCount();
                $config['per_page'] = 50;
                $choice = $config["total_rows"] / $config["per_page"];
                $config["num_links"] = round($choice);
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['first_link'] = false;
                $config['last_link'] = false;
                $config['first_tag_open'] = '<li class="paginate_button">';
                $config['first_tag_close'] = '</li>';
                $config['next_link'] = 'NEXT';
                $config['next_tag_open'] = '<li class="paginate_button next">';
                $config['next_tag_close'] = '</li>';
                $config['prev_link'] = 'PREVIOUS';
                $config['prev_tag_open'] = '<li class="paginate_button previous">';
                $config['prev_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li class="paginate_button">';
                $config['last_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li class="paginate_button">';
                $config['num_tag_close'] = '</li>';
                $this->pagination->initialize($config);
                $data['pagination_links'] = $this->pagination->create_links();
                $page = 0;
                $data['total_db_rows'] = $this->admin_model->userCount();
                $where = '';
                $data['user_role_list'] = $this->admin_model->get_user_role($page, $config['per_page'], $where);
                $data['tab_name'] = $tab;
            }
            if($tab == 'set-notification'){
                $data['total_db_rows']='';
                $data['tab_name'] = $tab;
            }
            if($tab == 'vehicle-pricing'){
                $config = array();
                $config['base_url'] = base_url() . 'admin/setting/vehicle-pricing';
                $config['total_rows'] = $this->admin_model->userCount();
                $config['per_page'] = 50;
                $choice = $config["total_rows"] / $config["per_page"];
                $config["num_links"] = round($choice);
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['first_link'] = false;
                $config['last_link'] = false;
                $config['first_tag_open'] = '<li class="paginate_button">';
                $config['first_tag_close'] = '</li>';
                $config['next_link'] = 'NEXT';
                $config['next_tag_open'] = '<li class="paginate_button next">';
                $config['next_tag_close'] = '</li>';
                $config['prev_link'] = 'PREVIOUS';
                $config['prev_tag_open'] = '<li class="paginate_button previous">';
                $config['prev_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li class="paginate_button">';
                $config['last_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="paginate_button"><a href="#">';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li class="paginate_button">';
                $config['num_tag_close'] = '</li>';
                $this->pagination->initialize($config);
                $data['pagination_links'] = $this->pagination->create_links();
                $page = 0;
                $data['total_db_rows'] = $this->admin_model->userCount();
                $where = '';
                $data['vehicle_price_list'] = $this->admin_model->getVihiclePricing($page, $config['per_page'], $where);
                $data['tab_name'] = $tab;
            }
            if($tab == 'vehicle-pricing-add'){
                $data['total_db_rows']='';
                $data['tab_name'] = 'vehicle-pricing-add';
            }
            $this->load->view('common/header', $data);
            $this->load->view('setting');
            $this->load->view('common/footer');
        }else{
            redirect('cRJDJK845-DFdx3');
        }
    }
    public function set_password(){
        $uName = $this->input->post('uname');
        $current_password = md5(trim($this->input->post('current_password')));
        $new_pass_encrypt = md5(trim($this->input->post('new_password')));
        $where = array('uName' => $uName, 'uPassword' => $current_password);
        $chckUser = $this->admin_model->GetInfoByRow('user','uID', $where);
        $errorM = '';
        if(!empty($chckUser)){
            //reset password
            $form_data = array(
                'uPassword' => $new_pass_encrypt
                );
            $this->admin_model->Manage('user',$form_data, $where, 'update');
            $response=array( 'status'=>'success', 'message'=>'Successfully Reset password');
        }else{
            $response=array( 'status'=>'error', 'message'=>'Current password you entered is invalid');
        }
        $this->session->set_flashdata($response);
        redirect('admin/setting/set-password');
    }
    function random_password() 
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $password = array(); 
        $gen = array();
        $alpha_length = strlen($alphabet) - 1; 
        for ($i = 0; $i < 8; $i++) 
        {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        $gen['gen_pass'] = implode($password);
        echo json_encode($gen);
    }
    public function addVehiclePrice(){
        $vehicle_cat = $this->input->post('vehicle_cat');
        $istkm = $this->input->post('1stkm');
        $extrakm = $this->input->post('extrakm');
        $waiting = $this->input->post('waiting');
        foreach ($vehicle_cat as $key => $val) {
            $form_data = array(
                'vehicle_cat' => $val,
                'pdFirstKm' => $istkm[$key],
                'pdAddKm' => $extrakm[$key],
                'pdWaitingMin' => $waiting[$key]
            );
            $this->admin_model->SaveForm('price_master_details',$form_data);
        }
        redirect('admin/setting/vehicle-pricing');
    }
    public function edit_vehicle_pricing($id=null){
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $where = array('pdID' => $id);
        $data['vehicle_pricing_edit'] = $this->admin_model->GetAllInfo('price_master_details','pdID',$where);
        $this->load->view('edit_vehicle_pricing', $data);
    }
    public function editvihiclePricing(){

        $form_data = array(
            'pdFirstKm' =>$this->input->post('1stkm'),
            'pdAddKm' =>$this->input->post('extrakm'),
            'pdWaitingMin' =>$this->input->post('waiting')
        );
        $hddpdID = $this->input->post('hddpdID');
        $where = array('pdID' => $hddpdID);
        $this->admin_model->Manage('price_master_details',$form_data, $where, 'update');
        $response=array( 'status'=>'success', 'message'=>'Successfully Update');
        $this->session->set_flashdata($response);
        redirect('admin/setting/vehicle-pricing');
    }
}