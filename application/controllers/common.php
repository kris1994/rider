<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {
    function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Colombo");
        $this->load->helper('path');
        $this->load->model('common_model');
    }



    // upload location img
    function addDocumentImg(){
        if (isset($_POST['fileName']) && $_POST['fileData'])
        {
            // Save uploaded file
            $uploadDir = 'uploads/document_img/';
            file_put_contents(
                $uploadDir. $_POST['fileName'],
                base64_decode($_POST['fileData'])
            );

        }
        return true;
    }





}