<?php

class Common_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        date_default_timezone_set("Asia/Colombo");
    }

    // edit profile
    function profileNotification($user){

        $query = $this->db->query("SELECT DISTINCT t.tDeviceID FROM user u JOIN user_notification n JOIN token t WHERE u.uID = $user AND u.uID = n.uID AND t.tAuthToken !='0' AND  t.uID = u.uID AND n.ntID = '5' AND n.ntStatus = '1' ");
        $result=$query->result_array();

        foreach ($result as $row){
            // android
            $message = "You have change your profile details";
            $data = array(
                'title'=> '',
                'body'=> $message,
                'sound' => 'default',
                'badge' =>1);
            $extra = array(
                'sound' => 'default',
                'badge' =>1,
                'type' =>'profile-update',
            );

            $url = 'https://fcm.googleapis.com/fcm/send';
            $server_key = 'AIzaSyBatbKyhd3vrGWeHRszM-HMUTNBShIDelE';
            $fields = array();
            $fields['to'] = $row['tDeviceID'];
            $fields['notification'] = $data;
            $fields['data'] = $extra;
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key='.$server_key
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_exec($ch);
            curl_close($ch);
        }
        return true;
    }
    // edit profile image
    function imageNotification($user){

        $query = $this->db->query("SELECT DISTINCT t.tDeviceID FROM user u JOIN user_notification n JOIN token t WHERE u.uID = $user AND u.uID = n.uID AND t.tAuthToken !='0' AND  t.uID = u.uID AND n.ntID = '5' AND n.ntStatus = '1' ");
        $result=$query->result_array();

        foreach ($result as $row){
            // android
            $message = "You have change your profile image";
            $data = array(
                'title'=> '',
                'body'=> $message,
                'sound' => 'default',
                'badge' =>1);
            $extra = array(
                'sound' => 'default',
                'badge' =>1,
                'type' =>'profile-update',
            );

            $url = 'https://fcm.googleapis.com/fcm/send';
            $server_key = 'AIzaSyBatbKyhd3vrGWeHRszM-HMUTNBShIDelE';
            $fields = array();
            $fields['to'] = $row['tDeviceID'];
            $fields['notification'] = $data;
            $fields['data'] = $extra;
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key='.$server_key
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_exec($ch);
            curl_close($ch);
        }
        return true;
    }





}