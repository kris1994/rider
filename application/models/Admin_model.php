<?php

class Admin_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
        date_default_timezone_set("Asia/Colombo");
    }

    // all spots
    function getAllPassenger()
    {
        $this->db->select('*');

        $sql = $this->db->get('user');
        return $sql->num_rows();
    }

    function getAllRider()
    {
        $this->db->select('*');

        $sql = $this->db->get('rider');
        return $sql->num_rows();
    }
    function userCount(){
        $this->db->select('*');
        $this->db->where("(utID = '1' OR utID = '2' OR utID = '4')");
        $sql = $this->db->get('user');
        return $sql->num_rows();
    }
    function getAllRiders($page, $limit, $where=null){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('*');
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->limit($limit, $start);
        $sql = $this->db->get('rider');
        $result = $sql->result_array();
        // $this->debug($result, 1);
        return $result;
    }
    function getVihiclePricing($page, $limit, $where=null){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('*');
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->limit($limit, $start);
        $sql = $this->db->get('price_master_details');
        $result = $sql->result_array();
        // $this->debug($result, 1);
        return $result;
    }
    function getRide_history($page, $limit, $whereDate=null){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('u.uID, u.uJoinDate, u.Uname as passenger, r.rName as rider, sum(rh.totalAmount) as Fare,
                            concat(sum(rh.totalDistance), " Km") as Distance, sum(rh.ratingParticularTrip) as rate');
        $this->db->from('user u');
        $this->db->join('ride_history rh', ' u.uID = rh.uID', 'left');
        $this->db->join('rider r', 'rh.rID = r.rID', 'left');
        if(!empty($whereDate)){
            $this->db->where($whereDate);
        }
        $this->db->group_by('rh.uID');
        $this->db->limit($limit, $start);
        $sql = $this->db->get();
        $result = $sql->result_array();
        // $this->debug($result, 1);
        return $result;
    }
    function getPayments($page, $limit, $wheredate){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('u.uID, `u`.`uJoinDate`, `u`.`uName`, `u`.`uEmail`, "" as `account_no`, ifnull(sum(rh.totalAmount), 0) as total_earning, ifnull(sum(rh.totalAmount)/2, 0) as bt_commision, ifnull(sum(rh.totalAmount)/2, 0) as rider_earning');
        $this->db->from('user u');
        $this->db->join('ride_history rh', ' u.uID = rh.uID', 'left');
        if(!empty($wheredate)){
            $this->db->where($wheredate);
        }
        $this->db->group_by('u.uID');
        $this->db->limit($limit, $start);
        $sql = $this->db->get();
        $result = $sql->result_array();
        // $this->debug($result, 1);
        return $result;
    }
    function getAllRidersPending($page, $limit, $wherenotin, $wheredate=null){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('r.*, count(rd.rdID) as no_of_documents');
        $this->db->from('rider r');
        $this->db->join('rider_document rd', 'r.rID = rd.rID','left');
        if(!empty($wheredate)){
            $this->db->where($wheredate);
        }
        if(!empty($wherenotin)){
            foreach ($wherenotin as $val) {
                if($val['no_of_doc'] == 7){
                    $this->db->where_not_in('r.rID',$val['rdoc_ID']);
                }
            }
        }
        
        $this->db->group_by('r.rID');
        $this->db->limit($limit, $start);
        $result = $this->db->get()->result_array();
        // $this->debug($result, 1);
        return $result;
    }
    function getAllRiderStatus3(){
        $sql = $this->db->query("SELECT rID as rdoc_ID, count(rdID) as no_of_doc FROM `rider_document` WHERE rdStatus=3
                group by rID")->result_array();
        return $sql;
    }
    function getAllUser($page, $limit){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('*');
        $this->db->limit($limit, $start);
        $sql = $this->db->get('bikes');
        $result = $sql->result_array();
        return $result;
    }

    function getInternalRider()

    {

        $this->db->select('*');
        $this->db->from('rider r');
        $this->db->join('bikes b', 'r.rName=b.bAssignRider', 'left');
        $this->db->where('b.bAssignRider is null');
        $this->db->group_by('r.rName');
        $result = $this->db->get()->result_array();
        // $this->debug($result, 1);
        return $result;
    }
     private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
    // function getInternalRider()

    // {

    //     $this->db->select('*');

    //     $sql = $this->db->get('rider');

    //     $result = $sql->result_array();
    //     return $result;
    // }
   function getInternalBike()
    {
        $sql = $this->db->query("SELECT *
                                FROM `bikes`
                                WHERE `bVehicleNumber` NOT IN(select rAssignBike from rider
                                where rAssignBike != ''
                                group by rAssignBike)
                                GROUP BY `bVehicleNumber`");
        $result = $sql->result_array();
        return $result;
    }
    function sortNormalUserCount($date){
        if($date =='' || $date ==null){
            $where = '';
        }else{
            $where = 'AND date(rJoinDate) = "'.$date.'"';
        }
        $sql = $this->db->query("SELECT * FROM rider WHERE rtID = '3' $where ORDER BY rID DESC ");
        return $sql->num_rows();

    }
    function normalUserCount($status){
        $this->db->select('*');
        $this->db->where("rtID", 3);
        if($status != 'All' && $status !=null && $status !=2){
            $this->db->where('rStatus', $status);
        }elseif ($status ==2){
            $this->db->where('rStatus', 0);
        }
        $this->db->order_by('rID', "DESC");
        $sql = $this->db->get('rider');
        return $sql->num_rows();
    }
    function userStatus($status=NULL){
        if ($status) {
            $this->session->set_userdata('userStatus', $status);
            return $status;
        } elseif ($this->session->userdata('userStatus')) {
            $status = $this->session->userdata('userStatus');
            return $status;
        } else {
            $status = '';
            return $status;
        }
    }


    function sortAllNormalUser($page, $limit,$date){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }

        if($date =='' || $date ==null){
            $where = '';
        }else{
            $where = 'AND date(rJoinDate) = "'.$date.'"';
        }
        $query = $this->db->query("SELECT * FROM rider WHERE rtID = '3' $where ORDER BY rID DESC LIMIT $start, $limit");
        $result=$query->result_array();
        return $result;
    }

    function addBikes(){
        $area = $this->input->post('internal_riders');
        $query = $this->db->query("SELECT * FROM rider");
        if($query->num_rows()>0) {
            $name = $query->row()->rName;
        }
        $bikes = array(

            'bID'=>  '',


            'bBrand' => $this->security->xss_clean($this->input->post('brand')),
            'vehicle_cat' => $this->security->xss_clean($this->input->post('vehicles')),

            'bType' => $this->security->xss_clean($this->input->post('type')),

            'bChassisNumber' => $this->security->xss_clean($this->input->post('chassis_number')),

            'bVehicleNumber' => $this->security->xss_clean($this->input->post('vehicle_number')),
            'bInsuranceExpiry' => $this->security->xss_clean($this->input->post('insurance_exp')),
            'bAssignRider'=>  $this->security->xss_clean($this->input->post('internal-rider')),

//            'lUrl' => $this->security->xss_clean($this->input->post('url')),
//
//            'lLatitude' => $this->security->xss_clean($this->input->post('latitude')),
//
//            'lLongitude' => $this->security->xss_clean($this->input->post('longitude')),
//
//            'lCurrency' => $this->security->xss_clean($this->input->post('currency')),
//
//            'lStatus'=>'0',
//
//            'bInsuranceExpiry' => date('Y-m-d'),



        );

        $this->db->insert('bikes', $bikes);

        $lid = $this->db->insert_id();
        $insert_id = $this->db->insert_id();

        return $lid;

    }
    public function delete_user($id)
    {
        $this->db->where('bID', $id);
        return $this->db->delete('bikes');
    }
    public function delete_news($id)
    {
        $this->db->where('bID', $id);
        return $this->db->delete('bikes'); // $this->db->delete('news', array('id' => $id));

        // error() method will return an array containing its code and message
        // $this->db->error();
    }
    public function get_news_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('bikes');
            return $query->result_array();
        }

        $query = $this->db->get_where('bikes', array('id' => $id));
        return $query->row_array();
    }

    function delete($id)
    {
        $this->db->where('bID', $id);
        $this->db->delete('bikes');
    }
    public function book_delete($bikeId)
    {
        $this->db->where('bID',$bikeId);
        $this->db->delete('bikes');
    }
    // add doc image

    function getLocationDetails($location){
        $query = $this->db->query("SELECT * From rider");
        $result=$query->result_array();
        return $result;
    }

    function getBikeDetails($location){
        $query = $this->db->query("SELECT * From bikes");
        $result=$query->result_array();
        return $result;
    }

    function addDocumentImg($lid)
    {
        $j=1;
        $s = 0;
        if (isset($_FILES['image'])) {
            $status = 2;
            for ($i = 0; $i < count($_FILES['image']['name']); $i++) {
                
                $randomNum = uniqid();
                $files = basename($_FILES['image']['name'][$i]);
                $tmp = explode('.', $files);
                $ext = end($tmp);
                $file = $randomNum . '.' . $ext;
                
                if($_FILES['image']['size'][$i] > 0){
                    $s += count($files);
                }

                $driving_exp = $this->input->post('driving_expe');

                $uploadDir = 'uploads/document_img/';
                $uploadFile = $uploadDir . $file;
                if (move_uploaded_file($_FILES['image']['tmp_name'][$i], $uploadFile)) {
                    // Prepare remote upload data
                    $uploadRequest = array(
                        'fileName' => basename($uploadFile),
                        'fileData' => base64_encode(file_get_contents($uploadFile))
                    );
                    
                    if($j==1){
                        $add_img = array(
                            'rID' => $lid,
                            'subID'=>$j,
                            'rdImage' => $file,
                            'rdStatus' => 1,
                            'rdExDate' => (!empty(@$driving_exp[$i]) ? @$driving_exp[$i] : ' ')
                        );
                    }
                    else{
                        $add_img = array(
                            'rID' => $lid,
                            'subID'=>$j,
                            'rdImage' => $file,
                            'rdStatus' => 1,
                            'rdExDate' => (!empty(@$driving_exp[$i]) ? @$driving_exp[$i] : ' ')

                        );
                    }

                    $rest = $this->db->insert('rider_document', $add_img);
                }
                $j++;
            }
            //update status = 2 if all documents submitted
            //for Under Review 
            if($s == 7){
                $form_data = array('rdStatus' => 2);
                $where_ar = array('rID' => $lid);
                $this->Manage('rider_document',$form_data, $where_ar, 'update');
            }

        }
    }
    function getDocumentImages($location){
        $images = $this->db->query("SELECT * FROM rider_document WHERE rdID =  '$location' ");
        $result=$images->result_array();
        return $result;
    }


    function addLocationImg1($lid){
        if(isset($_FILES['image'])) {
            for ($i = 0; $i < count($_FILES['image']['name']); $i++) {
                $randomNum = uniqid();
                $files = basename($_FILES['image']['name'][$i]);
                $tmp = explode('.', $files);
                $ext = end($tmp);
                $file = $randomNum . '.' . $ext;

                $target_dir = "uploads/document_img/temp/";
                $target_file = $target_dir . $file;
                $new_target = "uploads/document_img/";
                $uploadOk = 1;
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif"
                ) {
                    $uploadOk = 0;
                }

                if ($uploadOk == 1) {
                    if (move_uploaded_file($_FILES["image"]["tmp_name"][$i], $target_file)) {
                        $this->thumbnail($file, $target_dir, $new_target, 1032, 581 );
                        $add_img = array(
                            'rID' => $lid,
                            'rdImage' => $file,
                            'rdExDate' => $this->input->post('driving_exp'),
                        );
                        $this->db->insert('rider_document', $add_img);
                    }
                }
            }
        }
        return true;
    }

    function addRider(){
        $area = $this->input->post('internal_bikes');
        $query = $this->db->query("SELECT * FROM bikes ");
        if($query->num_rows()>0) {
            $name = $query->row()->bVehicleNumber;
            $id = $query->row()->bID;
//            $monday = $this->input->post('external_rider');
        }


        $equipments = $this->input->post('equipments');
        $equip = '';
        foreach ($equipments as $eq) {
            $equip .= $eq.',';
        }

        $bikes = array(
            'rID'=>  '',
            'rtID'=> 1,
            'bID'=>  $id,
            'rName' => $this->security->xss_clean($this->input->post('rider_name')),
            'rPhone' => $this->security->xss_clean($this->input->post('contact_no')),
            'rLastName' => $this->security->xss_clean($this->input->post('last_name')),
            'rEmail' => $this->security->xss_clean($this->input->post('rider_email')),
            'rJoinDate' => date('Y-m-d'),
            // 'rAssignBike'=>  $name,
            'vehicle_type'=>  $this->security->xss_clean($this->input->post('vehicle_type')),
            'rAssignBike'=>  $this->security->xss_clean($this->input->post('vehicles')),
            'rInitialDeposite' => $this->security->xss_clean($this->input->post('initial_deposit')),
            'rEmiNumber' => $this->security->xss_clean($this->input->post('phone_emi')),
            'rBykerPhoneNumber' => $this->security->xss_clean($this->input->post('byker_phone_number')),
            'rEquipments' => $this->security->xss_clean(rtrim($equip, ',')),
            'rSalary' => $this->security->xss_clean($this->input->post('rider_salary')),
            'rEpfNumber' => $this->security->xss_clean($this->input->post('rider_epfno')),
            'rInExRiders' => $this->input->post('rInExRiders'),
        );
        $this->db->insert('rider', $bikes);

        $lid = $this->db->insert_id();


        return $lid;


    }
    function searchUser($search_user=NULL){
        if ($search_user) {
            $this->session->set_userdata('search_user', $search_user);
            return $search_user;
        } elseif ($this->session->userdata('search_user')) {
            $search_user = $this->session->userdata('search_user');
            return $search_user;
        } else {
            $search_user = '';
            return $search_user;
        }
    }
    function searchBike($search_user=NULL){
        if ($search_user) {
            $this->session->set_userdata('search_rider', $search_user);
            return $search_user;
        } elseif ($this->session->userdata('search_user')) {
            $search_user = $this->session->userdata('search_user');
            return $search_user;
        } else {
            $search_user = '';
            return $search_user;
        }
    }
    // rider search
    function search($search=NULL){
        if ($search) {
            $this->session->set_userdata('search', $search);
            return $search;
        } elseif ($this->session->userdata('search')) {
            $search = $this->session->userdata('search');
            return $search;
        } else {
            $search = '';
            return $search;
        }
    }

    function searchUserCount($search){

        $sql = $this->db->query("SELECT DISTINCT rID as rider_id, rider.* FROM rider WHERE (rName LIKE  '%$search%' OR rEmail LIKE  '%$search%' ) AND rtID = '1' ORDER BY rID DESC ");
        return $sql->num_rows();

    }


    function getSearchCount($search){
        $query = $this->db->query("SELECT DISTINCT rID as rid, rider.* FROM rider WHERE (rName LIKE  '%$search%' OR rID LIKE  '%$search%' OR rEmail LIKE '%$search%') ");
        return $query->num_rows();
    }
    function searchUserAutoComplete($search){
        $query = $this->db->query("SELECT DISTINCT rID as rider_id, IF(rider.rName !='',rider.rName, rider.rEmail ) as rName FROM rider WHERE (rName LIKE  '%$search%' OR rEmail LIKE  '%$search%' ) AND rtID = '3' ORDER BY rID DESC LIMIT 10");
        $result=$query->result_array();
        return $result;
    }

    function searchAllNormalUser($page, $limit,$search){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }

        $query = $this->db->query("SELECT DISTINCT rID as rid, rider.* FROM rider WHERE (rName LIKE '%$search%' OR rID LIKE '%$search%' OR rEmail LIKE '%$search%') LIMIT $start, $limit");
        $result=$query->result_array();
        return $result;
    }
    function searchAllPassenger($page, $limit, $search){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('u.uID, u.uName, u.uPhone, u.uJoinDate, ur.urRate as rating');
        $this->db->from('user u');
        $this->db->join('user_review ur', 'u.uID = ur.uID', 'left');
        $this->db->like('u.uName', $search);
        $this->db->or_like('u.uPhone', $search);
        $this->db->or_like('u.uJoinDate', $search);
        $this->db->or_like('ur.urRate', $search);
        $this->db->where('u.uStatus', 0);
        $this->db->limit($limit, $start);
        $sql = $this->db->get();
        $result = $sql->result_array();
        return $result;
    }
    // bikes search
    function searchBikeCount($search){

        $sql = $this->db->query("SELECT DISTINCT bID as bike_id, bikes.* FROM bikes WHERE (bAssignRider LIKE  '%$search%' OR bVehicleNumber LIKE  '%$search%' ) ORDER BY bID DESC ");
        return $sql->num_rows();

    }
    function searchBikeAutoComplete($search){
        $query = $this->db->query("SELECT DISTINCT bID as bike_id, IF(bikes.bVehicleNumber !='',bikes.bVehicleNumber, bikes.bAssignRider ) as bVehicleNumber FROM bikes WHERE (bVehicleNumber LIKE  '%$search%' OR bAssignRider LIKE  '%$search%' )  ORDER BY bID DESC LIMIT 10");
        $result=$query->result_array();
        return $result;
    }

    function searchAllNormalBikes($page, $limit,$search){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }

        $query = $this->db->query("SELECT DISTINCT bID as bid, bikes.* FROM bikes WHERE (bAssignRider LIKE '%$search%' OR bID LIKE '%$search%' OR bVehicleNumber LIKE '%$search%') LIMIT $start, $limit");
        $result=$query->result_array();
        return $result;
    }

    function searchAutoComplete($term){
        $query = $this->db->query("SELECT DISTINCT rID as rid, rider.*   FROM rider WHERE (rName LIKE '%$term%' OR rID LIKE '%$term%' OR rEmail LIKE '%$term%') LIMIT 10");
        $result=$query->result_array();
        return $result;

    }
     public function GetInfoByRow($table,$order_by,$data = null) {
        $this->db->order_by($order_by, 'ASC');
        $this->db->where($data);
        $query = $this->db->get($table)->row();
        // $this->debug($query,1);
        return $query;
        
    }
    public function GetAllInfo($table,$order_by,$data = null, $num = null, $offset = null) {
        $this->db->order_by($order_by, 'ASC');
        if ($data != null)
            $this->db->where($data);
        return $this->db->get($table, $num, $offset)->result_array();
    }
    public function Manage($table,$data, $where, $type) {
        $this->db->where($where);
        if ($this->db->update($table, $data))
            return True;
        else
            return False;
    }
    public function RDelete($table,$data) {
        if ($this->db->delete($table, $data))
            return "successfully removed";
        else
            return "deletion unsuccessful";
    }
    public function SaveForm($table,$form_data) {
        $this->db->insert($table, $form_data);
        if ($this->db->affected_rows() == '1') {
            return $this->db->insert_id();
        }
        return FALSE;
    }
    public function getRiderHistory($where=null){
        $sql= $this->db->query("SELECT count(tripID) as totalTrip,
                                date_format(rideDate, '%h %p') as rhourDay
                                from ride_history
                                $where
                                group by date_format(rideDate, '%h')
                                order by date_format(rideDate, '%h') ASC")->result_array();
        return $sql;
    }
    public function getDetailQuery($query){
        $sql = $this->db->query($query)->result_array();
        return $sql;
    }
    public function getListPassenger($page, $limit, $where=null){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('u.uID, u.uName, u.uPhone, u.uJoinDate, ur.urRate as rating');
        $this->db->from('user u');
        $this->db->join('user_review ur', 'u.uID = ur.uID', 'left');
        if(!empty($where)){
            $this->db->where($where);    
        }
        $this->db->where('u.uStatus', 0);
        $this->db->limit($limit, $start);
        $sql = $this->db->get();
        $result = $sql->result_array();
        // $this->debug($result, 1);
        return $result;
    }
    public function get_ride_history_view($page, $limit, $where =null, $groupby=null){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('`r`.`rID`, `r`.`rName`, r.rAssignBike as vehicle_no, ifnull(sum(rh.totalAmount), 0) as fare,
                        concat(round(ifnull(sum(rh.totalDistance), 0),2)," Km") as distance, rh.uID,
                        rh.totalRideTime as duration');
        $this->db->from('rider r');
        $this->db->join('ride_history rh', ' r.rID = rh.rID', 'left');
        $this->db->where($where);
        if(!empty($groupby)){
            $this->db->group_by($groupby);    
        }
        
        $this->db->limit($limit, $start);
        $sql = $this->db->get();
        $result = $sql->result_array();
        // $this->debug($sql, 1);
        return $result;
    }
    public function get_planned_history_view($page, $limit, $where =null, $groupby=null){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('
                        `r`.`rID`, `r`.`rName`, `r`.`rAssignBike` as `vehicle_no`, 
                            ifnull(sum(sr.ridecost), 0) as fare,
                            sr.rideDistance as distance, `sr`.`uID`, `sr`.`rideTime` as `duration`
                         ');
        $this->db->from('rider r');
        $this->db->join('schedule_ride sr', ' `r`.`rID` = `sr`.`rID` ', 'left');
        $this->db->where($where);
        if(!empty($groupby)){
            $this->db->group_by($groupby);    
        }
        
        $this->db->limit($limit, $start);
        $sql = $this->db->get();
        $result = $sql->result_array();
        // $this->debug($sql, 1);
        return $result;
    }
    function get_user_role($page, $limit, $where=null){
        if($page >1){
            $pgs= $page - 1;
            $start = 50 * $pgs;
        }elseif($page == 1){
            $start = 0;
        }else{
            $start = 0;
        }
        $this->db->select('*');
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->limit($limit, $start);
        $sql = $this->db->get('user');
        $result = $sql->result_array();
        // $this->debug($result, 1);
        return $result;
    }
    function getVehicleNo($vt, $nt){
        $query = $this->db->query("SELECT vehicle_cat, bVehicleNumber from bikes
                  WHERE vehicle_cat ='$vt'
                  AND bVehicleNumber NOT IN ($nt)")->result_array();
        return $query;
    }
}
