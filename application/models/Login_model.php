<?php

class Login_model extends CI_Model
{
    public function validate()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $this->db->where('uEmail', $email);
        $this->db->where('uPassword', md5($password));
        $this->db->where('uStatus', 1);
        $this->db->where("(utID = '1' OR utID = '2' OR utID = '4')");
        $query = $this->db->get('user');

        if ($query->num_rows() == 1) {
            $row = $query->row();
            // $this->debug($row,1);
            $data = array(
                'admin_user' => $row->uID,
                'admin_username' => $row->uName,
                'user_type' => $row->utID,
                'user_email' => $row->uEmail,
                'validated' => true
            );
            $this->session->set_userdata($data);
            return true;
        }else{
            return false;
        }

    }
    private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
    function checkDeactivate(){
        $email = $this->input->post('email');
        $this->db->where('uEmail', $email);
        $this->db->where('uStatus', 0);
        $this->db->where("(utID = '1' OR utID = '2' OR utID = '4')");
        $sql = $this->db->get('user');
        return $sql->num_rows();
    }

    function activateAdmin(){
        $data = array(
            'uStatus' => 1,
        );
        $this->db->where('uID', $this->input->post('user'));
        $this->db->where('uStatus',0);
        $this->db->where("(utID = '1' OR utID = '2' OR utID = '4')");
        $query = $this->db->update('user', $data);
        if($query) {
            return true;
        } else {
            return false;
        }
    }

    function deactivateAdmin(){
        $data = array(
            'uStatus' => 0,
        );
        $this->db->where('uID', $this->input->post('user'));
        $this->db->where('uStatus', 1);
        $this->db->where("(utID = '1' OR utID = '2' OR utID = '4')");
        $query = $this->db->update('user', $data);
        if($query) {
            return true;
        } else {
            return false;
        }
    }

    function checkPass($pass, $userId) {
        $this->db->where('uID', $userId)->where('uPassword', md5($pass));
        $sql = $this->db->get('user');
        if($sql->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function changePasswordAction($userId) {
        $data = array(
            'uPassword' => md5($this->security->xss_clean($this->input->post('new_password'))),
        );
        $this->db->where('uID', $userId);
        $query = $this->db->update('user', $data);
        if($query) {
            return true;
        } else {
            return false;
        }
    }

    function addUser(){
        date_default_timezone_set("Asia/Colombo");
        $user = array(
            'utID'=> $this->security->xss_clean($this->input->post('admin_type')),
            'uName' => $this->security->xss_clean($this->input->post('username')),
            'uEmail' => $this->security->xss_clean($this->input->post('email')),
            'uPassword' => md5($this->security->xss_clean($this->input->post('password'))),
            'uJoinDate' => date('Y-m-d H:i:s'),
            'uStatus'=>'1',
        );
        $this->db->insert('user', $user);
        $uid = $this->db->insert_id();
        $random_number = intval( rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9). rand(1,9).rand(1,9) );
        $rand = 'auth_'.$random_number.$uid;
        $token = array(
            'uID'=> $uid,
            'tDeviceToken' => '0',
            'tAuthToken' => $rand,
            'tDeviceID' => '0',
        );
        $this->db->insert('token', $token);
        $profile = array(
            'uID'=> $uid,
        );
        $this->db->insert('user_profile', $profile);
        return true;
    }

    // check email exist or not
    function checkEmail()
    {
        $this->db->select('uName');
        $this->db->from('user');
        $this->db->where('uEmail', $this->input->post('email'));
        return $this->db->get()->result_array();
    }

    function checkEmailUser($uid){
        $this->db->select('uName');
        $this->db->from('user');
        $this->db->where('uID !=', $uid);
        $this->db->where('uEmail', $this->input->post('email'));
        return $this->db->get()->result_array();
    }

    function userData($uid){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('uID', $uid);
        return $this->db->get()->result_array();
    }


    function editUserInfo(){
        if($this->input->post('password') !=null || $this->input->post('password') !=''){
            $user = array(
                'utID'=> $this->security->xss_clean($this->input->post('admin_type')),
                'uName' => $this->security->xss_clean($this->input->post('username')),
                'uEmail' => $this->security->xss_clean($this->input->post('email')),
                'uPassword' => md5($this->security->xss_clean($this->input->post('password'))),
            );
        }else{
            $user = array(
                'utID'=> $this->security->xss_clean($this->input->post('admin_type')),
                'uName' => $this->security->xss_clean($this->input->post('username')),
                'uEmail' => $this->security->xss_clean($this->input->post('email')),
            );
        }
        $this->db->where('uID', $this->input->post('user'));
        $query = $this->db->update('user', $user);
        if($query) {
            return true;
        } else {
            return false;
        }
    }

}

