<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Admin/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['cRJDJK845-DFdx3'] = "Admin/index";
$route['admin/all-bikes'] = "Admin/allBikes";
$route['admin/all-bikes/(:num)'] = "Admin/allBikes";
$route['admin/all-bikes/(:any)'] = "Admin/allBikes";

$route['admin/all-riders'] = "Admin/allRider";
$route['admin/all-riders/(:num)'] = "Admin/allRider";
$route['admin/all-riders/(:any)/(:any)'] = "Admin/allRider";

$route['admin/all-locations/'] = "Admin/allLocation";
$route['admin/all-locations/(:num)'] = "Admin/allLocation";
$route['admin/sort-by-user-type'] = "Admin/sortByType";
$route['admin/sort-by-user-type/(:num)'] = "Admin/sortByType";
$route['admin/sort-by-user-type/(:any)'] = "Admin/sortByType";

$route['admin/sort-by-price'] = "Admin/sortByPrice";
$route['admin/sort-by-price/(:num)'] = "Admin/sortByPrice";
$route['admin/sort-by-price/(:any)'] = "Admin/sortByPrice";

$route['admin/create-user'] = "Admin/createUser";
$route['admin/edit-user/(:num)'] = "Admin/editUser";
$route['admin/all-reservation'] = "Admin/allReservation";
$route['admin/all-reservation/(:num)'] = "Admin/allReservation";
$route['admin/all-reservation/(:any)'] = "Admin/allReservation";
$route['admin/all-bikes'] = "Admin/allUser";
$route['admin/all-bikes/(:num)'] = "Admin/allUser";
$route['admin/all-bikes/(:any)'] = "Admin/allUser";
$route['admin/view-user'] = "Admin/viewUser";
$route['admin/view-user/(:num)'] = "Admin/viewUser";
$route['admin/view-user/(:any)'] = "Admin/viewUser";
$route['admin/sort-user'] = "Admin/sortUser";
$route['admin/sort-user/(:num)'] = "Admin/sortUser";
$route['admin/sort-user/(:any)'] = "Admin/sortUser";
$route['admin/search-user/(:num)'] = "Admin/searchUser";
$route['admin/search-user/(:any)'] = "Admin/searchUser";
$route['admin/search-user'] = "Admin/searchUser";

$route['admin/search-bike/(:num)'] = "Admin/searchBike";
$route['admin/search-bike/(:any)'] = "Admin/searchBike";
$route['admin/search-bike'] = "Admin/searchBike";

$route['admin/parking-owners'] = "Admin/parkingOwners";
$route['admin/parking-owners/(:num)'] = "Admin/parkingOwners";
$route['admin/parking-owners/(:any)'] = "Admin/parkingOwners";

$route['admin/sort-all-locations'] = "Admin/sortLocation";
$route['admin/sort-all-locations/(:num)'] = "Admin/sortLocation";
$route['admin/sort-all-locations/(:any)'] = "Admin/sortLocation";
$route['admin/sort-pending-locations'] = "Admin/sortPendingLocation";
$route['admin/sort-pending-locations/(:num)'] = "Admin/sortPendingLocation";
$route['admin/sort-pending-locations/(:any)'] = "Admin/sortPendingLocation";
$route['admin/fake-emails'] = "Admin/fakeEmail";
$route['admin/fake-emails/(:num)'] = "Admin/fakeEmail";
$route['admin/fake-emails/(:any)'] = "Admin/fakeEmail";

$route['admin/delete/(:any)'] = 'Admin/delete/$1';
$route['admin/approved-locations'] = "Admin/approvedLocation";
$route['admin/approved-locations/(:num)'] = "Admin/approvedLocation";
$route['admin/approved-locations/(:any)'] = "Admin/approvedLocation";
$route['admin/pending-approval'] = "Admin/pendingLocation";
$route['admin/pending-approval/(:num)'] = "Admin/pendingLocation";
$route['admin/pending-approval/(:any)'] = "Admin/pendingLocation";
$route['admin/contributed-locations'] = "Admin/contributedLocation";
$route['admin/contributed-locations/(:num)'] = "Admin/contributedLocation";
$route['admin/contributed-locations/(:any)'] = "Admin/contributedLocation";
$route['admin/search-locations'] = "Admin/searchLocation";
$route['admin/search-locations/(:num)'] = "Admin/searchLocation";
$route['admin/search-locations/(:any)'] = "Admin/searchLocation";
$route['admin/push-notification'] = "Admin/pushNotification";

$route['admin/view-bike/(:num)'] = "Admin/viewBike";
$route['admin/edit-rider/(:num)'] = "Admin/editRider";
$route['admin/pending-rider/(:num)'] = "Admin/pendingRider";
$route['admin/action-pending-rider/(:num)'] = "Admin/ActionpendingRider";
$route['admin/edit-location/(:num)'] = "Admin/editLocation";
$route['admin/location-reviews'] = "Admin/allReview";
$route['admin/location-reviews/(:num)'] = "Admin/allReview";
$route['admin/location-reviews/(:any)'] = "Admin/allReview";
$route['account-verify/(:any)'] = "Common/verifyEmail";
$route['activate-account'] = "Common/activateAccount";

$route['admin/edit-rates-by-area'] = "Admin/editRatesByLocationArea";
$route['admin/edit-rates-by-id'] = "Admin/editRatesByLocationId";
$route['admin/all-area'] = "Admin/allArea";
$route['admin/all-area/(:num)'] = "Admin/allArea";
$route['admin/all-area/(:any)'] = "Admin/allArea";

$route['help'] = "Landing/help";
$route['terms'] = "Landing/terms";
$route['contact'] = "Landing/contact";
$route['feedback'] = "Landing/feedback";
$route['privacy'] = "Landing/privacy";


$route['share-location/(:any)'] = "Common/shareLocation";
$route['share-locations/(:any)'] = "Common/loadShare";

$route['admin/search-bike-rider/(:num)'] = "Admin/searchBikeAndRider";
$route['admin/view-passenger/(:num)'] = "Admin/viewPassenger";
$route['admin/view-passenger/(:num)/(:any)'] = "Admin/viewPassenger";

$route['admin/setting/set-password/(:num)'] = "Admin/setting";
$route['admin/set-password'] = "Admin/set_password";
