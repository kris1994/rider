<style type="text/css">
    
.btn-circle {
  width: 10px;
  height: 10px;
  text-align: center;
  padding: 1px 0;
  font-size: 12px;
  line-height: 1.1;
  border-radius: 20px;
}

</style>
<link href="<?php echo base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <?php
            $segment = $this->uri->segment(3);
            if($segment == null) {
                $table_num_rows = 0;
                $showing = 1;
            } else if($segment == 1) {
                $table_num_rows = 0;
                $showing = 1;
            }elseif($segment > 1){
                $pgs= $segment - 1;
                $table_num_rows = 10 * $pgs;
                $showing = 10 * $pgs;
            }
            ?>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box no-padding">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40 no-padding">

                                <form autocomplete="on" id="form-send-location" method="post" action="<?php echo base_url() ?>admin/addPromocode" enctype="multipart/form-data">
                                <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-3">
                                                <fieldset class="form-group">
                                                        <label for="disabledTextInput"  style="color:black">Discount Type</label>
                                                         <div class="radio radio-warning radio-circle">
                                                            <input id="radio-12" type="radio" id="discount_type" name="discount_type" value="%">
                                                            <label class="col-lg-4" for="radio-12"> Percentage</label>
                                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                             <input id="radio-13" type="radio" id="discount_type" name="discount_type" value="lkr">
                                                            <label class="col-lg-4" for="radio-13"> Amount</label>
                                                        </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                            <div class="col-3">
                                                <fieldset class="form-group">
                                                    <label for="discount">Discount Value</label>
                                                    <input type="text" class="form-control" id="discount" placeholder="20%" name="discount" >
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Start Date</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" name="start_date" id="datepicker-autoclose" >
                                                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <fieldset class="form-group">
                                                    <label for="validNoRiders">Valid Number Of Riders</label>
                                                    <input type="text" class="form-control" id="validNoRiders" placeholder="10" name="validNoRiders" >
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                             <div class="col-3">
                                                <fieldset class="form-group">
                                                    <label for="promo_code">Promo Code</label>
                                                    <input type="text" class="form-control" id="promo_code" placeholder="Promo Code 001" name="promo_code" >
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Expiration</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" name="expiration" id="datepicker-autoclose" >
                                                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                             <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label style="color:black" for="TextInput4"><b>Message</b></label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-7">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Type Your Message</label>
                                                    <div class="input-group">
                                                       <textarea name="message" class="form-control" rows="4"></textarea>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label style="color:black" for="TextInput4"><b>Recipients</b></label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-2">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Select Filter Type</label>
                                                    <div class="input-group">
                                                       <select name="recipients" id="recipients" class="form-control">
                                                           <option value="Joined Date">Joined Date</option>
                                                           <option value="Ride Total">Ride Total</option>
                                                           <option value="Number of Rides">Number of Rides</option>
                                                           <option value="Rating">Rating</option>
                                                       </select>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
         <!--                                    <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div> -->
                                            <!-- <div class="row"> -->
                                            <!-- <div class="col-2" id="joinedate">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">&nbsp;</label>
                                                </fieldset>
                                            </div> -->
                                             <div class="col-4" id="joinedate_filter">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">&nbsp;</label>
                                                    <div class="input-group input-daterange " style="border:1px solid #ccc;">
                                                    <input id="startDate1" name="startDate1" type="text" class="form-control dateranger" readonly="readonly" placeholder="From" style="text-align: left">
                                                    <span class="input-group-addon">
                                                    <span class="zmdi zmdi-calendar color-change">
                                                    </span></span>
                                                    <input id="endDate1" name="endDate1" type="text" class="form-control dateranger" readonly="readonly" placeholder="To" style="text-align: left">
                                                    <span class="input-group-addon daterange-span-radius">
                                                    <span class="zmdi zmdi-calendar color-change"></span></span>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        <!-- </div> -->
                                            <div class="col-2" id="ride_total_filter">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Ride Total(LKR)</label>
                                                    <div class="input-group">
                                                       <input type="text" name="ride_total" class="form-control" value="0">
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                            <div class="col-2" id="rating_filter">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Rating</label>
                                                    <div class="input-group">
                                                       <input type="text" name="rating" class="form-control" value="0">
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                             <div class="col-2" id="all_passenger_filter">
                                                <fieldset class="form-group">
                                                    <label for="TextInput">All Passenger</label><br>
                                                    <label class="switch">
                                                        <input class="switch-input" type="checkbox" name="all_passenger" />
                                                        <span class="switch-label" data-on="On" data-off="Off"></span>
                                                        <span class="switch-handle"></span>
                                                    </label>

                                                    <!-- <input type="hidden" id="type" name="rInExRiders" value="External"> -->
                                                </fieldset>
                                            </div>
                                            
                                        </div>
                                        
                                          <div class="row pull-right">
                                            <div class="divider"></div>
                                                <div class="form-group text-right m-b-0">
                                                    <input name="btn_save" class="btn btn-default m-l-5" type="submit" value="Discard">
                                                    <input name="btn_save" class="btn btn-warning m-l-5" type="submit" value="Done">
                                                </div>
                                         </div>
                                </form>
                            </div><!-- end col -->

                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>

        <!-- /.content -->
    </div>

            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">

                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Promo Code</th>
                                <th>Discount</th>
                                <th>Start Date</th>
                                <th>Expiration</th>
                                <th>Status</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($promo_list as $p) {
                                        if($p['status'] == 1){ // Active
                                            $status = '<span class="btn btn waves-effect waves-light btn-circle" style="background-color:#00FA9A"></span> <label> Active </label>';
                                        }
                                        if($p['status'] == 2){ // Pending
                                            $status = '<span class="btn btn waves-effect waves-light btn-circle" style="background-color:#CD5C5C"></span> <label> Pending </label>';
                                        }
                                        if($p['status'] == 3){ // Expired
                                            $status = '<span class="btn btn-default waves-effect waves-light btn-circle" style="background-color:#ccc"></span> <label> Expired </label>';
                                        }
                                ?>
                                <tr>
                                    <td><?php echo $p['promoCode']?></td>
                                    <td><?php echo $p['discountAmount']?></td>
                                    <td><?php echo $p['startDate']?></td>
                                    <td><?php echo $p['expiration']?></td>
                                    <td><?php echo $status;?></td>
                                    <td>
                                        <a href="javascript:void(0)" onclick="delete_data(<?php echo $p['id']?>)" title='draft'><i class="zmdi zmdi-delete"></i></a>
                                        <!-- <a href="<?php //echo base_url('admin/draftPromo/').$p['idpc']?>" title='draft' ><i class="zmdi zmdi-delete"></i></a> -->
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <!-- /pagination and status -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->


<!-- Sweet Alert js -->
<script src="<?php echo base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- <script src="<?php echo base_url()?>assets/pages/jquery.sweet-alert.init.js"></script> -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $("#recipients").on("change", function(){
        if($(this).val()=='Ride Total'){
            $("#ride_total_filter").show();
            $("#joinedate_filter").hide();
             $("#rating_filter").hide();
        }
        if($(this).val()=='Number of Rides' || $(this).val()=='Joined Date'){
            $("#ride_total_filter").hide();
            $("#joinedate_filter").show();
             $("#rating_filter").hide();
        }
        if($(this).val()=='Rating'){
            $("#joinedate_filter").hide();
            $("#rating_filter").show();

        }
    })
    $(".radio-circle input").on('click',function(){
        if($(this).val() == '%'){
            $("#discount").attr('placeholder', '20%');
        }
        if($(this).val() == 'lkr'){
            $("#discount").attr('placeholder', 'LKR');
        }
    })
    $(document).ready(function() {
        $("#ride_total_filter").hide();
        $("#all_passenger_filter").hide();
        $("#rating_filter").hide();
        $('.input-daterange input').each(function() {
            $(this).datepicker({
                autoclose: true,
                todayHighlight: true,
                format:'yyyy-mm-dd' 
            });
        });
    });
   
    function delete_data(_id){
        swal({
            title: "Are you sure you want to delete?",
            text: "You will not be able to recover this list!",
            type: "warning",
            showCancelButton: true,
            cancelButtonClass: 'btn-secondary waves-effect',
            confirmButtonClass: 'btn-warning',
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    data:{
                        id:_id
                    },
                    type: "post",
                    url: "<?php echo base_url()?>"+"admin/draftPromo",
                    success:function(data){
                        swal("Deleted!", "this promo code deleted.", "success");
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });
                
            } else {
                swal("Cancelled", "You Cancelled", "error");
            }
        });
    }
</script>
<script>
$(".datepicker").datepicker({
autoclose: true,
todayHighlight: true,
format:'yyyy-mm-dd'
});
    var resizefunc = [];
</script>
<script>
$(document).ready(function () {
    $('#datatable').DataTable();

    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
});

</script>
