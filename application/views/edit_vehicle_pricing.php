<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40">
                                <form method="post" action="<?php echo base_url('admin/editvihiclePricing')?>" autocomplete="off">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-box table-responsive table-margin">
                                            <?php if ($this->session->flashdata('status') =='success') { ?>
                                                <div class="alert alert-success" role="alert">
                                                    <strong>Success!</strong><?php echo $this->session->flashdata('message')?>
                                                </div>
                                            <?php } ?>
                                            <?php if ($this->session->flashdata('status') =='error') { ?>
                                                <div class="alert alert-danger" role="alert">
                                                    <strong>Error!</strong><?php echo $this->session->flashdata('message')?>
                                                </div>
                                            <?php } ?>
                                                <h1>Vehicle Pricing</h1>
                                                <?php
                                                foreach ($vehicle_pricing_edit as $c) {
                                                ?>
                                                <!-- <br> -->
                                                <div class="row">
                                                    <div class="col-12">
                                                            <hr class="style1">
                                                            <h5 style=" color: steelblue;"><?php echo $c['vehicle_cat']?></h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <label for="1stkm" class="col-md2">1st Km</label>
                                                                <input type="hidden" id="hddpdID" class="form-control" name="hddpdID" value="<?php echo $c['pdID']?>">
                                                                <input type="hidden" id="vehicle_cat" class="form-control" name="vehicle_cat" value="<?php echo $c['vehicle_cat']?>">
                                                                <input type="text" id="1stkm" class="form-control" name="1stkm" value="<?php echo $c['pdFirstKm']?>">
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-4">
                                                        <fieldset class="form-group">
                                                            <label for="extrakm">Extra Km</label>
                                                            <input type="text" class="form-control" id="extrakm" name="extrakm" value="<?php echo $c['pdAddKm']?>">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-4">
                                                        <fieldset class="form-group">
                                                            <label for="waiting">Waiting</label>
                                                            <input type="text" class="form-control" id="waiting" name="waiting" value="<?php echo $c['pdWaitingMin']?>">

                                                        </fieldset>
                                                    </div>
                                                </div> <!-- end row-->
                                                <?php }?>
                                                <div class="form-group m-b-0">
                                                    <input type="hidden" name="uname" value="<?php echo $this->session->userdata('admin_username')?>">
                                                    <button id="generate_pass" class="btn  btn-primary btn btn-save waves-effect m-l-5 " type="submit">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</div>
<!-- End content-page -->
