<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style type="text/css">
    hr.style1{
        border-top: 2px solid #ccc;
        margin-left: 10px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">

        <div class="container-fluid">
            <?php
            $segment = $this->uri->segment(3);
            if($segment == null) {
                $table_num_rows = 0;
                $showing = 1;
            } else if($segment == 1) {
                $table_num_rows = 0;
                $showing = 1;
            }elseif($segment > 1){
                $pgs= $segment - 1;
                $table_num_rows = 10 * $pgs;
                $showing = 10 * $pgs;
            }
            ?>
            <!-- end row -->

    <div class="col-md-6 col-xs-12 m-t-20">
        <ul class="nav nav-tabs m-b-10" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link <?php echo ($tab_name == 'set-password' || empty($tab_name)) ? 'active' : ''?>" id="password-tab" data-toggle="" href="<?php echo base_url('admin/setting/set-password')?>" role="tab" aria-controls="home" aria-expanded="true">Password</a>
            </li>
           <li class="nav-item">
                <a class="nav-link <?php echo ($tab_name == 'set-roles') ? 'active' : ''?>" id="set-roles-tab" data-toggle="" href="<?php echo base_url('admin/setting/set-roles')?>"
                   role="tab" aria-controls="profile">User Roles</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo ($tab_name == 'set-notification') ? 'active' : ''?>" id="set-notification-tab" data-toggle="" href="<?php echo base_url('admin/setting/set-notification/')?>"
                   role="tab" aria-controls="profile">Push Notification</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo ($tab_name == 'vehicle-pricing') ? 'active' : ''?>" id="set-notification-tab" data-toggle="" href="<?php echo base_url('admin/setting/vehicle-pricing/')?>"
                   role="tab" aria-controls="profile">Vehicle Pricing</a>
            </li>
        </ul>
    </div>
          
            <div class="tab-content" id="myTabContent">
            
            <?php if($tab_name == 'set-password' || empty($tab_name)){?>
            <form method="post" action="<?php echo base_url('admin/set-password')?>" autocomplete="off">
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive table-margin">
                        <?php if ($this->session->flashdata('status') =='success') { ?>
                            <div class="alert alert-success" role="alert">
                                <strong>Success!</strong><?php echo $this->session->flashdata('message')?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('status') =='error') { ?>
                            <div class="alert alert-danger" role="alert">
                                <strong>Error!</strong><?php echo $this->session->flashdata('message')?>
                            </div>
                        <?php } ?>
                             <div class="col-3">
                                <fieldset class="form-group">
                                <div class="form-group">
                                    <label for="current_password">Current Password</label>
                                    <input type="password" id="current_password" name="current_password" class="form-control" placeholder="" >
                                </div>
                                </fieldset>
                            </div>
                            <div class="col-3">
                                <fieldset class="form-group">
                                    <label for="new_password">New Password</label>
                                    <input type="text" class="form-control" id="new_password" name="new_password" required>
                                </fieldset>
                            </div>
                            <div class="form-group m-b-0">
                                <input type="hidden" name="uname" value="<?php echo $this->session->userdata('admin_username')?>">
                                <button id="generate_pass" class="btn  btn-primary btn btn-save waves-effect m-l-5 " type="button">Generate</button>
                                <button id="reset-pass" class="btn  btn-send btn btn-save waves-effect m-l-5 " type="submit">Reset Password</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php }?>
            <?php if($tab_name == 'set-roles'){?>
                <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Join Date</th>
                                <th class="display-none-div action-th">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (count($user_role_list) != 0): ?>
                                <?php foreach ($user_role_list as $value): ?>
                                    <tr>
                                        <td><?php echo $value['uID'] ?></td>
                                        <td><?php if($value['uName']){ echo $value['uName']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['uEmail']){ echo $value['uEmail']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['uJoinDate']){echo $value['uJoinDate']; echo '-';}?> </td>
                                        <td class="tbl-action">Edit
                                        </td>
                                    </tr>
                                    <?php $total_db_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!--End set roles -->
            <?php }?>
            <?php if($tab_name == 'set-notification'){?>
            <form method="post" action="">
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <fieldset class="form-group">
                            <label for="message" style="color: black; font-size: 17px;">Type Your Message</label>
                            <textarea class="form-control" rows="10"></textarea>
                        </fieldset>
                         <div class="row pull-right">
                            <div class="divider"></div>
                            <div class="form-group text-right m-b-0">
                                <a href="<?php echo base_url('admin/setting')?>" class="btn btn-cancel  waves-effect waves-light">Discard</a>
                                <button id="add-loc-btn" class="btn  btn-send btn btn-save waves-effect m-l-5" type="submit">Send</button>
                            </div>
                         </div>
                    </div>
                </div>
            </div> <!--End Tab Pending -->
            </form>
            <?php }?>
            <?php if($tab_name == 'vehicle-pricing'){?>

                <div class="row">
                    
                    <div class="col-12">
                        <div class="row pull-right">
                            <div class="divider"></div>
                            <div class="form-group text-right m-b-0">
                                <a href="<?php echo base_url()?>admin/setting/vehicle-pricing-add"><button type="button" class="btn btn-addnew " ><i class="zmdi zmdi-bike"></i>  Add New</button></a>
                            </div>
                         </div>
                    <div class="card-box table-responsive table-margin">

                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Vehicle Category</th>
                                <th>First Km</th>
                                <th>Add Km</th>
                                <th>Waiting Min</th>
                                <th class="display-none-div action-th">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (count($vehicle_price_list) != 0): ?>
                                <?php foreach ($vehicle_price_list as $value): ?>
                                    <tr>
                                        <td><?php echo $value['pdID'] ?></td>
                                        <td><?php echo $value['vehicle_cat'] ?></td>
                                        <td><?php echo $value['pdFirstKm']?></td>
                                        <td><?php echo $value['pdAddKm']?> </td>
                                        <td><?php echo $value['pdWaitingMin']?> </td>
                                        <td class="tbl-action">
                                            <a href="<?php echo base_url(); ?>admin/edit_vehicle_pricing/<?php echo $value['pdID'];?>" class="btn btn-danger" >Edit</a>
                                        </td>
                                    </tr>
                                    <?php $total_db_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!--End set roles -->
        <?php }?>
            <?php if($tab_name == 'vehicle-pricing-add'){?>
            <form method="post" action="<?php echo base_url('admin/addVehiclePrice')?>" autocomplete="off">
                <div class="row">
                    <div class="col-12">
                        <div class="card-box table-responsive table-margin">
                        <?php if ($this->session->flashdata('status') =='success') { ?>
                            <div class="alert alert-success" role="alert">
                                <strong>Success!</strong><?php echo $this->session->flashdata('message')?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('status') =='error') { ?>
                            <div class="alert alert-danger" role="alert">
                                <strong>Error!</strong><?php echo $this->session->flashdata('message')?>
                            </div>
                        <?php } ?>
                            <h1>Vehicle Pricing</h1>
                            <?php
                            $vehicle_cat = array('Car','Mini','Tuk','Bikes','Van');
                            foreach ($vehicle_cat as $c) {
                            ?>
                            <!-- <br> -->
                            <div class="row">
                                <div class="col-12">
                                        <hr class="style1">
                                        <h5 style=" color: steelblue;"><?php echo $c?></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="1stkm" class="col-md2">1st Km</label>
                                            <input type="hidden" id="vehicle_cat" class="form-control" name="vehicle_cat[]" value="<?php echo $c?>">
                                            <input type="text" id="1stkm" class="form-control" name="1stkm[]" >
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-4">
                                    <fieldset class="form-group">
                                        <label for="extrakm">Extra Km</label>
                                        <input type="text" class="form-control" id="extrakm" name="extrakm[]" >
                                    </fieldset>
                                </div>
                                <div class="col-4">
                                    <fieldset class="form-group">
                                        <label for="waiting">Waiting</label>
                                        <input type="text" class="form-control" id="waiting" name="waiting[]" >

                                    </fieldset>
                                </div>
                            </div> <!-- end row-->
                            <?php }?>
                            <div class="form-group m-b-0">
                                <input type="hidden" name="uname" value="<?php echo $this->session->userdata('admin_username')?>">
                                <button id="generate_pass" class="btn  btn-primary btn btn-save waves-effect m-l-5 " type="submit">Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php }?>
        </div>
        </div> <!-- end row -->
        <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->
</div>
<!-- End content-page -->

<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div>
<!-- END wrapper -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();

        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'colvis']
        });

        table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );

</script>
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>
<script>
    var resizefunc = [];
    $("#generate_pass").click(function(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "admin/random_password",
            dataType :'json',
            success: function (res) {
                $("#new_password").val(res['gen_pass']);
            }
        });
    })

</script>

<!-- Responsive examples -->
