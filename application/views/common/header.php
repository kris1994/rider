
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="BykerTaxi">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

    <!-- App title -->
    <title>Byker - Taxi</title>

    <!-- DataTables -->
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css">
    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
    <!-- Plugins css-->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>

    <!-- form Uploads -->
    <link href="<?php echo base_url(); ?>assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/css/sweetalert.css" rel="stylesheet" type="text/css" />
    <!-- Plugins css -->
    <link href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/mjolnic-bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- C3 charts css -->
    <link href="<?php echo base_url(); ?>assets/plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

    <!-- Switchery css -->
    <link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/simplePagination.css">
    <!-- App CSS -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
    <script>
        var base_url = "<?php echo base_url(); ?>";
    </script>
</head>


<body class="fixed-left body-main">
    

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="<?php echo base_url(); ?>admin/dashboard" class="logo">

                <span><img width="183" height="42" src="<?php echo base_url(); ?>assets/images/smlogo.png"></span></a>
            <!-- <a href="<?php echo base_url(); ?>admin/dashboard" class="logo"> -->
                <!-- <span id="visible-engrage"><img width="43" height="42" src="<?php echo base_url(); ?>assets/images/bike@2x.png"></span></a> -->
        </div>

        <nav class="navbar-custom">

            <ul class="list-inline float-right mb-0">
                <li class="list-inline-item">
                     <form autocomplete="off" id="search-all-form" method="post" action="<?php echo base_url() ?>admin/search-bike-rider/1">
                        <li class="hidden-mobile app-search">
                            <div class="input-group custom-input-group">
                                <select name="search_category" id="search_category" class="form-control" placeholder="Search User">
                                    <option value="bikes" <?php echo @($this->input->post('search_category') == 'bikes') ? 'selected' : ''?> >Bikes</option>
                                    <option value="riders" <?php echo @($this->input->post('search_category') == 'riders') ? 'selected' : ''?> >Riders</option>
                                    <option value="passenger" <?php echo @($this->input->post('search_category') == 'passenger') ? 'selected' : ''?> >Passenger</option>
                                </select>
                                <input type="hidden" name="search_param" value="name" id="search_param">
                                <input name="search" autocomplete="off" id="search-user" class="form-control" value="<?php echo $this->input->post('search')?>" placeholder="Search Bikes/Riders">
                                <!-- <button type="submit" class="button-insider-search"><i class="fa fa-search"></i></button> -->
                            </div>

                        </li>
                    </form>
                </li>
                <li class="list-inline-item dropdown notification-list notification-border">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
<!--                        <img src="--><?php //echo base_url(); ?><!--assets/images/users/user-image-menu.png" alt="user" class="rounded-circle">-->
                        <span class="admin_username hidden-xs"><?= $this->session->userdata['admin_username'] ?></span><i class="zmdi zmdi-chevron-down ico-deco"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow"><small>Welcome ! <?= $this->session->userdata['admin_username'] ?></small> </h5>
                        </div>

                        <!-- item-->
                        <a href="<?php echo base_url(); ?>change-password" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-account-circle"></i> <span>Change Password</span>
                        </a>


                        <a href="<?= base_url() ?>admin/logout" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-power"></i> <span>Logout</span>
                        </a>

                    </div>
                </li>
                <li class="list-inline-item dropdown notification-list ">
                    <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        <i class="zmdi zmdi-notifications noti-icon"></i>
                        <span class="noti-icon-badge"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5><small><span class="label label-danger pull-xs-right">7</span>Notification</small></h5>
                        </div>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                            <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1min ago</small></p>
                        </a>

                        <!-- All-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                            View All
                        </a>

                    </div>
                </li>
            </ul>

            <ul class="list-inline menu-left mb-0">
                <div class="container-fluid">
                    <div class="col-sm-4">
                        <h4 class="page-title float-left">Dashboard</h4>


                        <div class="clearfix"></div>
                    </div>
                </div>
            </ul>

        </nav>

    </div>
    <!-- Top Bar End -->

<!-- ========== Left Sidebar Start ========== -->
<?php $this->load->view('common/sidebar.php')?>


    <script type="text/javascript">
    $("#search_category").on("change",function(){
        var _cat = $("#search_category").val();
        if(_cat == 'bikes'){
            $("#search-user").attr('placeholder','Search Bikes')
        }else if(_cat =='riders'){
            $("#search-user").attr('placeholder','Search Riders')
        }else{
            $("#search-user").attr('placeholder','Search Passenger')
        }
    })
</script>