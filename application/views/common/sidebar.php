<div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul class="list-inline menu-left mb-30">
                    <li class="float-right">
                        <button class="button-menu-mobile open-left waves-light waves-effect">
                            <i class="ion-chevron-left"></i>
                        </button>
                    </li>

                </ul>
                <ul>
                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/dashboard" class="waves-effect"><i class="zmdi zmdi-equalizer"></i><span> Dashboard </span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/all-bikes/1" class="waves-effect"> <i class="zmdi zmdi-bike"></i><span> Bikes</span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="<?php echo base_url('admin/passenger');?>" class="waves-effect"> <span><i class=" zmdi zmdi-airline-seat-recline-normal"></i> Passengers </span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="<?php echo base_url(); ?>admin/all-riders/1" class="waves-effect"><i class="zmdi zmdi-account"></i><span> Riders </span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="<?php echo base_url('admin/promo_codes');?>" class="waves-effect"><i class="zmdi zmdi-local-activity"></i> <span> Promo Codes </span> </a>
                    </li>
                    <li class="text-muted menu-title">More</li>
                    <li class="has_sub">
                        <a href="<?php echo base_url('admin/setting')?>" class="waves-effect"><i class="zmdi zmdi-settings"></i><span> Settings </span></a>
                    </li>
                    <li class="has_sub">
                        <a href="<?php echo base_url() ?>admin/logout" class="waves-effect"><i class="zmdi zmdi-power"></i></i><span>Log Out</span> </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>