
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-lg-12 col-xl-12">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <ul class="icons-list day-list">
                                    <li class="dropdown text-muted">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="week-base week-base-show-prt">Today</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-menu-right change-week-prt">
                                            <li><a href="javascript:void(0)" id="show-today-prt" >Today</a></li>
                                            <li><a href="javascript:void(0)" id="show-week-prt" >Week</a></li>
                                            <li><a href="javascript:void(0)" id="show-month-prt" >Month</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <h4 class="header-title m-t-0 m-b-20">Peek Ride Time</h4>
                            <!-- <div id="morris-bar-stacked" style="height: 320px;"></div> -->
                            <div id="peek-ride-time" style="height: 320px;"></div>
                        </div>
                    </div><!-- end col-->
                </div>
                <!-- end row -->

                <div class="row">
                        <div class="card-box tilebox-one" style="width: 15%">
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Rides This Week</h6>
                            <h2 class="m-b-20 counter-text-r" data-plugin=""><?php echo $getRideThisWeek?></h2>
                        </div>
                        <div class="card-box tilebox-one" style="width: 24%">
                            <div class="dropdown pull-right">
                                <ul class="icons-list day-list">
                                    <li class="dropdown text-muted">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="week-base week-base-show-re">Today</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-menu-right change-week-re">
                                            <li><a href="javascript:void(0)" id="show-today" >Today</a></li>
                                            <li><a href="javascript:void(0)" id="show-week" >Week</a></li>
                                            <li><a href="javascript:void(0)" id="show-month" >Month</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Earnings This Week</h6>
                            <h2 class="m-b-20 counter-text-etw"><span data-plugin=""><?php echo $getEarningThisWeek?></span></h2>
                        </div>
                    <!-- </div> -->
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">

                        <div class="card-box tilebox-one">
                            <!-- <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/passender-ic.png"> -->
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Passengers Count<i class="pull-right zmdi zmdi-airline-seat-recline-normal"></i></h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup"><?php echo $total; ?></h2>
                        </div>

                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">
                        <div class="card-box tilebox-one">
                            <!-- <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/ride-ic.png"> -->
                            <h6 class="title-card-text text-muted text-uppercase m-b-20"> Riders Count<i class="pull-right ion-ios7-person"></i></h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup"><?php echo $total_rider; ?></h2>

                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3  custom-col">
                        <div class="card-box tilebox-one">
                            <!-- <img class="float-right text-muted"  src="<?php echo base_url(); ?>assets/images/ratings-ic.png"> -->
                            <h6 class="title-card-text text-muted text-uppercase m-b-20">Ratings <i class="pull-right ion-android-star"></i></h6>
                            <h2 class="m-b-20 counter-text" data-plugin="counterup"><?php echo number_format($getTotalRatings,1);?></h2>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-lg-12 col-xl-12 m-t-20">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <ul class="icons-list day-list">
                                    <li class="dropdown text-muted">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="week-base week-base-show-earn"><?php echo date('Y')?></span><span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-menu-right change-week-earn">
                                            <li><a href="javascript:void(0)" id="show-month" ><?php echo date('Y')?></a></li>
                                            <?php
                                            $months = array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
                                            foreach ($months as $m) {
                                            ?>
                                            <li><a href="javascript:void(0)" id="show-month" ><?php echo $m?></a></li>
                                            <?php }?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <h4 class="header-title m-t-0">Earnings</h4>
                            <div class="p-20">
                                <div id="line-regions-x"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- end col-->
        </div>
        <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->
</div>


</div>
<!-- END wrapper -->
<script>


// bar chart
$(function() {
  // Create a function that will handle AJAX requests
  function requestData(rideWeekStat, chart){
    $.ajax({
      type: "GET",
      dataType: 'json',
      url: "<?php echo site_url('admin/getRiderHistory') ?>", // This is the URL to the API
      data: { rideWeekStat: rideWeekStat }
    })
    .done(function( data ) {
      // When the response to the AJAX request comes back render the chart with new data
      chart.setData(data);
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      alert( "error occured" );
    });
  }

  var chart = Morris.Bar({
    // ID of the element in which to draw the chart.
    element: 'peek-ride-time',
    data: [0, 0], // Set initial data (ideally you would provide an array of default data)
    xkey:'y',
    ykeys:['a'],
    labels:['Amount'],
    hideHover:'auto',
    barColors: ["#ff8000"],
    gridLineColor: ['#1bb99a'],
    stacked:true
  });

  // Request initial data for the past 7 rideWeekStat:

  requestData('Today', chart);

  $(".change-week-prt").on("click","li a",function(e){
      e.preventDefault();
        $(".week-base-show-prt").text($(this).text());
        rideWeekStat = $(this).text();
        requestData(rideWeekStat, chart);
  });

  // Earnings -------------------------------------------

    function requestDataEarn(earnMonthStat, chartE){
        $.ajax({
              type: "GET",
              dataType: 'json',
              url: "<?php echo site_url('admin/getEarningPerMonth') ?>", // This is the URL to the API
              data: { earnMonthStat: earnMonthStat }
            })
            .done(function( data ) {
              // When the response to the AJAX request comes back render the chartE with new data
              chartE.setData(data);
            })
            .fail(function() {
              // If there is no communication between the server, show an error
              alert( "error occured" );
            });
    }
  
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var chartE = Morris.Line({
        element: 'line-regions-x', 
        behaveLikeLine: true,
        parseTime : false, 
        data: [0, 0], // Set initial data (ideally you would provide an array of default data)
        xkey: 'm',
        ykeys: ['a'],
        labels: ['Earning'],
        pointFillColors: ['#707f9b'],
        pointStrokeColors: ['#ffaaab'],
        lineColors: ['#f26c4f'],
        redraw: true
     });
    
    var currentTime = new Date()
    // returns the year (four digits)
    var year = currentTime.getFullYear()
    requestDataEarn(year, chartE);

    $(".change-week-earn").on("click","li a",function(e){
        $('.week-base-show-earn').text($(this).text());
        earnMonthStat = $(this).text();
        requestDataEarn(earnMonthStat, chartE);
    });
});

</script>
<script>
    $(".change-week-re").on("click","li a",function(){
        $(".week-base-show-re").text($(this).text());
         $.ajax({
            type: "POST",
            dataType: 'json',
            url: "<?php echo base_url(); ?>" + "admin/getRiderEarnings",
            data: { REweekStat: $(this).text()},
            success: function (res) {
                $(".counter-text-r").text(res[0]['totalRE']);
                $(".counter-text-etw").text(res[0]['totalEarning']);
            }
        });
    })
    var resizefunc = [];
</script>

