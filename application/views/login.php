
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="row no-gutters">
    <div class="col-6 col-md-4"></div>
    <div class="col-12 col-sm-6 col-md-8">
        <div class="wrapper-page">

            <div class="account-bg">
                <div class="mb-0">
                    <div class="text-center m-t-20">
                        <a href="<?php echo base_url()?>" class="logo">
                            <span><img src="<?php echo base_url(); ?>images/bykes_rider.png" style="width: 40%"></span>
                        </a>
                    </div>
                    <div class="m-t-10 p-20">

                                    <?php if ($this->session->flashdata('errors')): ?>
                                        <div class="display-message-text text-center">
                                            <?php echo $this->session->flashdata('errors'); ?>
                                        </div>
                                    <?php endif; ?>

                                    <form class="m-t-20"  action="<?php echo base_url()?>Admin/checkLogin" method="post">
                                        <div class="form-group has-feedback col-12 text-center">
                                            <label>Email address</label>
                                            <input type="email" name="email" class="form-control form-control-login"  placeholder="Enter email">
                                        </div>
                                        <div class="form-group has-feedback col-12 text-center">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control form-control-login" placeholder="Password">
                                        </div>
                                        <div class="form-group text-center row m-t-30">
                                            <div class="col-12">
                                                <button class="btn btn-login btn-success btn-block waves-effect waves-light" type="submit">Log In</button>
                                            </div>
                                        </div>
                                    </form>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end card-box-->

            <div class="m-t-20">
                <div class="text-center">
                    <p class="text-white">Don't have an account? <a href="pages-register.html" class="text-white m-l-5"><b>Sign Up</b></a></p>
                </div>
            </div>

        </div></div>

</div>
<!-- end wrapper page -->

