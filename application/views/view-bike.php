
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">


            <!-- end row -->


            <div class="row">
                <div class="col-12">
                    <div class="card-box no-padding">



                        <div class="row">


                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40 no-padding">

                                <form autocomplete="on" id="form-send-location" method="post" action="<?= base_url() ?>admin/addLocation1" enctype="multipart/form-data">
                                    <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-6">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <label for="disabledTextInput">Biker ID</label>
                                                        <p>
                                                            <?php if($location[0]['bID']){
                                                                echo $location[0]['bID'];
                                                            }else{
                                                                echo '-';
                                                            } ?>
                                                        </p>
                                                    </div>

                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Vehicle Number</label>
                                                    <p>
                                                        <?php if($location[0]['bVehicleNumber']){
                                                            echo $location[0]['bVehicleNumber'];
                                                        }else{
                                                            echo '-';
                                                        } ?>
                                                    </p>
                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Brand</label>
                                                    <p>
                                                        <?php if($location[0]['bBrand']){
                                                            echo $location[0]['bBrand'];
                                                        }else{
                                                            echo '-';
                                                        } ?>
                                                    </p>
                                                </fieldset>

                                            </div>

                                            <div class="col-6">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Chassis Number</label>
                                                    <p>
                                                        <?php if($location[0]['bChassisNumber']){
                                                            echo $location[0]['bChassisNumber'];
                                                        }else{
                                                            echo '-';
                                                        } ?>
                                                    </p>
                                            </div>
                                            <div class="col-6">
                                                <fieldset class="form-group">
                                                    <label for="TextInput2">Type</label>
                                                    <p>
                                                        <?php if($location[0]['bType']){
                                                            echo $location[0]['bType'];
                                                        }else{
                                                            echo '-';
                                                        } ?>
                                                    </p>

                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <fieldset class="form-group">
                                                    <label for="TextInput2">Insurance Expiry</label>
                                                    <p>
                                                        <?php if($location[0]['bInsuranceExpiry']){
                                                            echo $location[0]['bInsuranceExpiry'];
                                                        }else{
                                                            echo '-';
                                                        } ?>
                                                    </p>

                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <fieldset class="form-group">
                                                    <label for="TextInput2">Assign Rider</label>
                                                    <p>
                                                        <?php if($location[0]['bAssignRider']){
                                                            echo $location[0]['bAssignRider'];
                                                        }else{
                                                            echo '-';
                                                        } ?>
                                                    </p>

                                                </fieldset>
                                            </div>

                                        </div>

                                    </div>





                                </form>
                            </div><!-- end col -->

                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->



        </div> <!-- container -->

    </div> <!-- content -->



</div>
<!-- End content-page -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->
<div class="side-bar right-bar">
    <div class="nicescroll">
        <ul class="nav nav-pills nav-justified text-xs-center">
            <li class="nav-item">
                <a href="#home-2"  class="nav-link active" data-toggle="tab" aria-expanded="false">
                    Activity
                </a>
            </li>
            <li class="nav-item">
                <a href="#messages-2" class="nav-link" data-toggle="tab" aria-expanded="true">
                    Settings
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade active show" id="home-2">
                <div class="timeline-2">
                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">5 minutes ago</small>
                            <p><strong><a href="#" class="text-info">John Doe</a></strong> Uploaded a photo <strong>"DSC000586.jpg"</strong></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">30 minutes ago</small>
                            <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">59 minutes ago</small>
                            <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">1 hour ago</small>
                            <p><strong><a href="#" class="text-info">John Doe</a></strong>Uploaded 2 new photos</p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">3 hours ago</small>
                            <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <small class="text-muted">5 hours ago</small>
                            <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="messages-2">

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Notifications</h5>
                        <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">API Access</h5>
                        <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Auto Updates</h5>
                        <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

                <div class="row m-t-10">
                    <div class="col-8">
                        <h5 class="m-0">Online Status</h5>
                        <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                    </div>
                    <div class="col-4 text-right">
                        <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" data-size="small"/>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end nicescroll -->
</div>
<!-- /Right-bar -->



</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>
<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

<!-- Autocomplete -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/jquery.mockjax.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/autocomplete/countries.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pages/jquery.autocomplete.init.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/pages/jquery.formadvanced.init.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/clockpicker/bootstrap-clockpicker.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>

<!-- file uploads js -->
<script src="<?php echo base_url(); ?>assets/plugins/fileuploads/js/dropify.min.js"></script>


<script>

    (function() {
        $(document).ready(function() {
            $('.switch-input').on('change', function() {
                var isChecked = $(this).is(':checked');
                var selectedData;
                var $switchLabel = $('.switch-label');
                console.log('isChecked: ' + isChecked);

                if(isChecked) {

                    $("#type").val("Internal");


                } else {
                    $("#type").val("External");
                }



            });

            // Params ($selector, boolean)
            function setSwitchState(el, flag) {
                el.attr('checked', flag);
            }

            // Usage
            setSwitchState($('.switch-input'), true);
        });

    })();


</script>
<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
</script>
<script type="text/javascript">
    function parkingFee() {
        if ($('#price').is(':checked')) {
            $('#price-details :input').attr('disabled', true);
        } else {
            $('#price-details :input').removeAttr('disabled');
        }
    }
</script>
<script>
    $('#add-tag').on('click', function (e) {
        if ($('#location-tags').val().trim() !== "") {
            var val = $('#location-tags').val();
            var tagHTML = '<div class="input-group tags-padding">' +
                '<input class="form-control" value="'+val+'" name="tags[]">' +
                '<span class="input-group-addon remove-tag"><i class="glyphicon glyphicon-remove"></i></span>' +
                '</div>';
            $('#show-tags').append(tagHTML);
            $('.remove-tag').on('click', function (e) {
                $(this).parent().remove();
            });
        }
        $('#location-tags').val('');
    });

</script>
<script>


    function removeImage(image,location){
        var id = "#remove-img-"+image;
        swal({
                title: "Are you sure?",
                text: "You want to Delete this?",
                type: "warning",   showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "<?= base_url() ?>owner/deleteLocationImage",
                        type: 'POST',
                        data: {image: image,location:location},
                        success: function (res) {
                            if(res){
                                $(id).hide();
                                $('#show-img').html('');
                                $('#show-img').html(res);
                                swal("Success!", "Successfully Deleted ", "success");
                                $('.dropify').dropify({
                                    error: {
                                        'fileSize': 'The file size is too big ({{ value }} max).',
                                        'minWidth': 'The image width is too small ({{ value }}}px min).',
                                        'maxWidth': 'The image width is too big ({{ value }}}px max).',
                                        'minHeight': 'The image height is too small ({{ value }}}px min).',
                                        'maxHeight': 'The image height is too big ({{ value }}px max).',
                                        'imageFormat': 'The image format is not allowed ({{ value }} only).'
                                    }
                                });
                            }else{

                            }

                        }
                    });
                }
            });
    }

</script>
<!---->
<!--</body>-->
<!--</html>-->
<!--<div class="content-wrapper">-->
<!--    <!-- Content Header (Page header) -->-->
<!--    <section class="content-header">-->
<!--        <h1>-->
<!--            Locations-->
<!--            <small>View Location</small>-->
<!--        </h1>-->
<!--        <ol class="breadcrumb">-->
<!--            <li><a href="--><?php //echo base_url()?><!--admin/edit-location/--><?//= $location[0]['rID'] ?><!--"><button type="button" class="btn btn-primary" >Edit</button></a></li>-->
<!--        </ol>-->
<!--    </section>-->
<!---->
<!--    <!-- Main content -->-->
<!--    <section class="content">-->
<!--        <div class="row">-->
<!--            <div class="col-xs-12">-->
<!--                <div class="box">-->
<!--                    <!-- /.box-header -->-->
<!--                    <div class="box-body">-->
<!---->
<!--                        <div class="col-md-12 col-sm-12 col-xs-12">-->
<!--                            <div class="x_panel">-->
<!--                                <div class="x_title">-->
<!--                                    <h2><i class="fa fa-map-marker"></i> Location Info </h2>-->
<!---->
<!--                                    <div class="clearfix"></div>-->
<!--                                </div>-->
<!--                                <hr />-->
<!--                                <div class="x_content">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Location Name - English</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['rName']){
//                                                    echo $location[0]['rName'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->

<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Location Name - Sinhala</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if(count($location_sinhala) >0){
//                                                    if($location_sinhala[0]['lName']){
//                                                        echo $location_sinhala[0]['lName'];
//                                                    }else{
//                                                        echo '-';
//                                                    }
//
//                                                } else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Location Name - Tamil</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if(count($location_tamil) >0){
//                                                    if($location_tamil[0]['lName']){
//                                                        echo $location_tamil[0]['lName'];
//                                                    }else{
//                                                        echo '-';
//                                                    }
//
//                                                } else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Location Owner / Added By</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['owner']){
//                                                    echo $location[0]['owner'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Location Area</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>--><?php //if($location[0]['lArea']){
//                                                    echo $location[0]['lArea'];
//                                                }else{
//                                                    echo '-';
//                                                } ?><!--</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr/>-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Parking Type</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>--><?php //if($location[0]['pkID'] == 1 ){
//                                                    echo "Private";
//                                                }elseif($location[0]['pkID'] == 2 ){
//                                                    echo "Street";
//                                                }elseif($location[0]['pkID'] == 3 ){
//                                                    echo "TPS";
//                                                } ?><!--</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr/>-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Landmark - English</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lLandmark']){
//                                                    echo $location[0]['lLandmark'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Landmark - Sinhala</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if(count($location_sinhala) >0){
//                                                    if($location_sinhala[0]['lLandmark']){
//                                                        echo $location_sinhala[0]['lLandmark'];
//                                                    }else{
//                                                        echo '-';
//                                                    }
//
//                                                } else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Landmark - Tamil</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if(count($location_tamil) >0){
//                                                    if($location_tamil[0]['lLandmark']){
//                                                        echo $location_tamil[0]['lLandmark'];
//                                                    }else{
//                                                        echo '-';
//                                                    }
//
//                                                } else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!---->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Address - English</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lAddress']){
//                                                    echo $location[0]['lAddress'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Address - Sinhala</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if(count($location_sinhala) >0){
//                                                    if($location_sinhala[0]['lAddress']){
//                                                        echo $location_sinhala[0]['lAddress'];
//                                                    }else{
//                                                        echo '-';
//                                                    }
//
//                                                } else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Address - Tamil</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if(count($location_tamil) >0){
//                                                    if($location_tamil[0]['lAddress']){
//                                                        echo $location_tamil[0]['lAddress'];
//                                                    }else{
//                                                        echo '-';
//                                                    }
//
//                                                } else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!---->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Special Notes - English</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lAbout']){
//                                                    echo nl2br($location[0]['lAbout']);
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Special Notes - Sinhala</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            <p>-->
<!--                                                --><?php //if(count($location_sinhala) >0){
//                                                    if($location_sinhala[0]['lAbout']){
//                                                        echo nl2br($location_sinhala[0]['lAbout']);
//                                                    }else{
//                                                        echo '-';
//                                                    }
//
//                                                } else{
//                                                    echo '-';
//                                                } ?>
<!---->
<!---->
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Special Notes - Tamil</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            <p>-->
<!--                                                --><?php //if(count($location_tamil) >0){
//                                                    if($location_tamil[0]['lAbout']){
//                                                        echo nl2br($location_tamil[0]['lAbout']);
//                                                    }else{
//                                                        echo '-';
//                                                    }
//                                                } else{
//                                                    echo '-';
//                                                } ?>
<!---->
<!---->
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <hr />-->
<!---->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>URL</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lUrl']){
//                                                    echo $location[0]['lUrl'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!---->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Latitude</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lLatitude']){
//                                                    echo $location[0]['lLatitude'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!---->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Longitude</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lLongitude']){
//                                                    echo $location[0]['lLongitude'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    --><?php //if($location[0]['pkID'] == 3): ?>
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-4 col-sm-4">-->
<!--                                                <p>Start Latitude</p>-->
<!--                                            </div>-->
<!--                                            <div class="col-md-8 col-sm-8 location-div">-->
<!--                                                <p>-->
<!--                                                    --><?php //if($location[0]['lStartLatitude']){
//                                                        echo $location[0]['lStartLatitude'];
//                                                    }else{
//                                                        echo '-';
//                                                    } ?>
<!--                                                </p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <hr />-->
<!---->
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-4 col-sm-4">-->
<!--                                                <p>Start Longitude</p>-->
<!--                                            </div>-->
<!--                                            <div class="col-md-8 col-sm-8 location-div">-->
<!--                                                <p>-->
<!--                                                    --><?php //if($location[0]['lStartLongitude']){
//                                                        echo $location[0]['lStartLongitude'];
//                                                    }else{
//                                                        echo '-';
//                                                    } ?>
<!--                                                </p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <hr />-->
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-4 col-sm-4">-->
<!--                                                <p>End Latitude</p>-->
<!--                                            </div>-->
<!--                                            <div class="col-md-8 col-sm-8 location-div">-->
<!--                                                <p>-->
<!--                                                    --><?php //if($location[0]['lEndLatitude']){
//                                                        echo $location[0]['lEndLatitude'];
//                                                    }else{
//                                                        echo '-';
//                                                    } ?>
<!--                                                </p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <hr />-->
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-4 col-sm-4">-->
<!--                                                <p>End Longitude</p>-->
<!--                                            </div>-->
<!--                                            <div class="col-md-8 col-sm-8 location-div">-->
<!--                                                <p>-->
<!--                                                    --><?php //if($location[0]['lEndLongitude']){
//                                                        echo $location[0]['lEndLongitude'];
//                                                    }else{
//                                                        echo '-';
//                                                    } ?>
<!--                                                </p>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <hr />-->
<!--                                    --><?php //endif; ?>
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Location Tags</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            --><?php //foreach ($tags as $tag): ?>
<!--                                                <p>-->
<!--                                                    --><?//= $tag['ltTag']?>
<!--                                                </p>-->
<!--                                            --><?php //endforeach; ?>
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Currency</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lCurrency']){
//                                                    echo $location[0]['lCurrency'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Status</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lStatus']==0){
//                                                    echo 'Draft';
//                                                }elseif($location[0]['lStatus']==1){
//                                                    echo 'Available';
//                                                }elseif($location[0]['lStatus']==2){
//                                                    echo 'Not Available';
//                                                } elseif($location[0]['lStatus']==3){
//                                                    echo 'On Vacation';
//                                                }?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Added</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lAdded']){
//                                                    echo $location[0]['lAdded'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4 location-div">-->
<!--                                            <p>Is Approved</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            <p> --><?php //if($location[0]['lIsApproved']==1){
//                                                    echo 'Approved';
//                                                }else{
//                                                    echo 'Not Approved';
//                                                } ?><!--</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!---->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Approved Date</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['lApproved']){
//                                                    echo $location[0]['lApproved'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Approved By</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['approved_by']){
//                                                    echo $location[0]['approved_by'];
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Is Contributed</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['IsContributed']==1){
//                                                    echo 'Yes';
//                                                }else{
//                                                    echo 'No';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Contributor</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8 location-div">-->
<!--                                            <p>-->
<!--                                                --><?php //if($location[0]['IsContributed']==1){
//                                                    if($location[0]['lContributor'] > 0){
//                                                        echo $location[0]['contributor'];
//                                                    }else{
//                                                        echo 'Anonymous Contributor';
//                                                    }
//                                                }else{
//                                                    echo '-';
//                                                } ?>
<!--                                            </p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row ">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Opening Hour - English</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            --><?php //if(count($opening_hour) >0){?>
<!--                                                <div class="col-sm-2">Monday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour[0]['ohMon'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Tuesday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour[0]['ohTue'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Wednesday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour[0]['ohWed'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Thursday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour[0]['ohThu'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Friday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour[0]['ohFri'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Saturday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour[0]['ohSat'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Sunday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour[0]['ohSun'] ?><!--</div>-->
<!--                                            --><?php //}else{
//                                                echo '-';
//                                            }?>
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Opening Hour - Sinhala</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            --><?php //if(count($opening_hour_sinhala) >0){?>
<!--                                                <div class="col-sm-2">Monday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_sinhala[0]['ohMon'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Tuesday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_sinhala[0]['ohTue'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Wednesday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_sinhala[0]['ohWed'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Thursday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_sinhala[0]['ohThu'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Friday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_sinhala[0]['ohFri'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Saturday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_sinhala[0]['ohSat'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Sunday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_sinhala[0]['ohSun'] ?><!--</div>-->
<!--                                            --><?php //}else{
//                                                echo '-';
//                                            }?>
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="row lang-space">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Opening Hour - Tamil</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            --><?php //if(count($opening_hour_tamil) >0){?>
<!--                                                <div class="col-sm-2">Monday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_tamil[0]['ohMon'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Tuesday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_tamil[0]['ohTue'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Wednesday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_tamil[0]['ohWed'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Thursday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_tamil[0]['ohThu'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Friday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_tamil[0]['ohFri'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Saturday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_tamil[0]['ohSat'] ?><!--</div>-->
<!--                                                <div class="col-sm-2">Sunday</div>-->
<!--                                                <div class="col-sm-4 location-div">--><?//= $opening_hour_tamil[0]['ohSun'] ?><!--</div>-->
<!--                                            --><?php //}else{
//                                                echo '-';
//                                            }?>
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Available Facility</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            --><?php //if(count($location_facility) >0){?>
<!--                                                --><?php //foreach ($location_facility as $row): ?>
<!--                                                    <div class="col-sm-4 location-div">--><?//= $row['fName'] ?><!--</div>-->
<!--                                                --><?php //endforeach; ?>
<!--                                            --><?php //}else{
//                                                echo '-';
//                                            }?>
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Location Images</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            --><?php //if(count($location_img) >0){?>
<!--                                                --><?php //foreach ($location_img as $row): ?>
<!--                                                    <div class="col-sm-6" style="padding-bottom: 20px">-->
<!--                                                        <img width="100%" height="175" src="--><?//= base_url() ?><!--uploads/location_img/--><?//= $row['liImage'] ?><!--" >-->
<!--                                                    </div>-->
<!--                                                --><?php //endforeach; ?>
<!--                                            --><?php //}else{
//                                                echo '-';
//                                            }?>
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-4 col-sm-4">-->
<!--                                            <p>Street View Image</p>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-8 col-sm-8">-->
<!--                                            --><?php //if($location[0]['lUrl']){
//                                                $st_val = explode(',', $location[0]['lUrl']);
//                                                $lat_val = explode('@', $st_val[0]);
//                                                $lat = $lat_val[1];
//                                                $long = $st_val[1];
//                                                $heads = explode('h', $st_val[4]);
//                                                $head = $heads[0];
//                                                ?>
<!--                                                <img class="street-view-img" width="100%" src="https://maps.googleapis.com/maps/api/streetview?size=640x360&location=--><?php //echo $lat; ?><!--,--><?php //echo $long; ?><!--&heading=--><?php //echo $head; ?><!--&key=AIzaSyCYNlFJ_Hhz7CIot7JeSEZwfwhhOfedUPE">-->
<!--                                            --><?php // }?>
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <hr />-->
<!--                                    <div id="antoox">-->
<!--                                        <div>-->
<!--                                            <div></div>-->
<!--                                            <div></div>-->
<!--                                            <div></div>-->
<!--                                        </div>-->
<!---->
<!--                                    </div>-->
<!--                                    --><?php //if($location[0]['lIsApproved']==1){ ?>
<!--                                        <div class="btn btn-primary btn-location">Approved</div>-->
<!--                                        --><?php //if($location[0]['lStatus']!=5){ ?>
<!--                                            <form method="post" action="--><?//= base_url()?><!--admin/deactivate">-->
<!--                                                <input type="hidden" name="location" value="--><?//= $location[0]['lID']; ?><!--">-->
<!--                                                <button class="btn btn-danger delete-location" type="submit">Deactivate</button>-->
<!--                                            </form>-->
<!--                                        --><?php //}else{ ?>
<!--                                            <div class="btn btn-success delete-location">Deactivated by Owner</div>-->
<!---->
<!--                                        --><?php //} ?>
<!--                                    --><?php //}else{ ?>
<!--                                    --><?php //if($location[0]['lStatus'] == 5){ ?>
<!--                                    <form method="post" action="--><?//= base_url()?><!--admin/giveApprovalSpot">-->
<!--                                        --><?php //}else{ ?>
<!--                                        <form method="post" action="--><?//= base_url()?><!--admin/giveApproval">-->
<!--                                            --><?php //} ?>
<!--                                            <input type="hidden" name="location" value="--><?//= $location[0]['lID']; ?><!--">-->
<!--                                            <button class="btn btn-primary btn-location" type="submit">Give Approval</button>-->
<!--                                        </form>-->
<!---->
<!--                                        --><?php //if($location[0]['lStatus']==4 && $location[0]['lStatus']!=5){ ?>
<!--                                            <form method="post" action="--><?//= base_url()?><!--admin/activateLocation">-->
<!--                                                <input type="hidden" name="location" value="--><?//= $location[0]['lID']; ?><!--">-->
<!--                                                <button class="btn btn-success delete-location" type="submit">Activate</button>-->
<!--                                            </form>-->
<!--                                        --><?php //} ?>
<!--                                        --><?php //if($location[0]['lStatus']!=4 && $location[0]['lStatus']!=5){ ?>
<!--                                            <form method="post" action="--><?//= base_url()?><!--admin/deactivate">-->
<!--                                                <input type="hidden" name="location" value="--><?//= $location[0]['lID']; ?><!--">-->
<!--                                                <button class="btn btn-danger delete-location" type="submit">Deactivate</button>-->
<!--                                            </form>-->
<!--                                        --><?php //} ?>
<!--                                        --><?php //} ?>
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                    <!-- /.box-body -->-->
<!--                </div>-->
<!--                <!-- /.box -->-->
<!---->
<!--            </div>-->
<!--            <!-- /.col -->-->
<!--        </div>-->
<!--        <!-- /.row -->-->
<!--    </section>-->
<!--    <!-- /.content -->-->
<!--</div>-->
<!---->
<!---->
<!---->
