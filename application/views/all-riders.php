<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style type="text/css">
.btn-circle {
  width: 10px;
  height: 8px;
  text-align: center;
  padding: 1px 0;
  font-size: 12px;
  line-height: 1.1;
  border-radius: 20px;
}

</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">

        <div class="container-fluid">
            <?php
            $segment = $this->uri->segment(3);
            if($segment == null) {
                $table_num_rows = 0;
                $showing = 1;
            } else if($segment == 1) {
                $table_num_rows = 0;
                $showing = 1;
            }elseif($segment > 1){
                $pgs= $segment - 1;
                $table_num_rows = 10 * $pgs;
                $showing = 10 * $pgs;
            }
            ?>
            <!-- end row -->

    <div class="col-md-6 col-xs-12 m-t-20">
        <ul class="nav nav-tabs m-b-10" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link <?php echo (empty($tab_name)) ? 'active' : ''?>" id="home-tab" data-toggle="" href="<?php echo base_url('admin/allRider/1')?>" role="tab" aria-controls="home" aria-expanded="true">Details</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo ($tab_name == 'pending') ? 'active' : ''?>" id="pending-tab" data-toggle="" href="<?php echo base_url('admin/allRider/1/pending')?>" role="tab" aria-controls="profile">Pending <span class="label label-danger pull-xs-right"><?php
                        $rdocID = $this->admin_model->getAllRiderStatus3();
                        $where = $rdocID;
                        echo count($this->admin_model->getAllRidersPending(0, '20000',$where))?></span></a>
            </li>
           <li class="nav-item">
                <a class="nav-link <?php echo ($tab_name == 'payments') ? 'active' : ''?>" id="payments-tab" data-toggle="" href="<?php echo base_url('admin/allRider/1/payments')?>"
                   role="tab" aria-controls="profile">Payments</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo ($tab_name == 'ride_history') ? 'active' : ''?>" id="ride-history-tab" data-toggle="" href="<?php echo base_url('admin/allRider/1/ride_history')?>"
                   role="tab" aria-controls="profile">Ride History</a>
            </li>
        </ul>
    </div>
            <section class="content-header">
                <ul class="list-inline menu-left mb-11">
                    <form autocomplete="off" id="search-all-form" method="post" action="<?php echo base_url() ?>admin/search-user/1">
                    <li class="hidden-mobile app-search">
                        <div class="input-group custom-input-group">
                            <input type="hidden" name="search_param" value="name" id="search_param">
                            <input name="search" autocomplete="off" id="search-user" class="form-control" placeholder="Search User">
                            <button type="submit" class="button-insider-search"><i class="fa fa-search"></i></button>
                        </div>
                    </li>
                    </form>
                </ul>

                <ol class="breadcrumb">
                    <div class="col-sm-9">
                    <form id="sort-submits" method="post" action="<?php echo base_url('admin/allRider/1/').$tab_name?>">
                        <div class="input-group input-daterange ">
                            <span class="input-group-addon daterange-div text-sm-dt">Date Joined</span>
                            <input id="startDate1" name="startDate1" type="text" value="<?php echo $fromdate?>" class="form-control dateranger" readonly="readonly" placeholder="From"> <span class="input-group-addon"> <span class="zmdi zmdi-calendar color-change"></span></span>
                            <input id="endDate1" name="endDate1" type="text" value="<?php echo $enddate?>" class="form-control dateranger" readonly="readonly" placeholder="To">
                            <span class="input-group-addon daterange-span-radius"> <span class="zmdi zmdi-calendar color-change"></span> </span>
                            <button type="submit" name="btn_search_date"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                    </div>
                    <br/>
                    <?php if($tab_name == 'payments'){?>
                    <form id="sort-submits-category" method="post" action="<?php echo base_url('admin/allRider/1/payments')?>">
                        <div class="col-sm-4 pull-right ">
                            <select class="form-control" name="datesearch" id="datesearch">
                                <option value="All Time">All Time</option>
                                <option value="Today" <?php echo ($datesearchcat == 'Today') ? 'selected' : ''?> >Today</option>
                                <option value="This Week" <?php echo ($datesearchcat == 'This Week') ? 'selected' : ''?>>This Week</option>
                                <option value="This Month" <?php echo ($datesearchcat == 'This Month') ? 'selected' : ''?>>This Month</option>
                            </select>
                        </div>
                    </form>
                    <?php }?>
                </ol>

                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url()?>admin/addRider"><button type="button" class="btn btn-addnew <?php echo (!empty($tab_name)) ? 'disabled' : ''?>" ><!--<img class="btn-bike-img"  src="<?php echo base_url(); ?>assets/images/btn-bike.png">--><i class="zmdi zmdi-bike"></i>Add New</button></a></li>
                </ol>
            </section>
            <div class="tab-content" id="myTabContent">
            
            <?php
            if($tab_name == 'pending'){?>
                <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Email</th>
                                <th>No of Documents</th>
                                <th class="display-none-div action-th">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (count($all_riders) != 0): ?>
                                <?php foreach ($all_riders as $value): ?>
                                    <tr>
                                        <td><?php echo $value['rID'] ?></td>
                                        <td><?php if($value['rName']){ echo $value['rName']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['rPhone']){ echo $value['rPhone']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['rEmail']){ echo $value['rEmail']; }else{ echo '-'; }?></td>
                                        <td><?phpif($value['no_of_documents']){echo $value['no_of_documents']; echo '-';}?> </td>
                                        <td class="tbl-action">
                                            <a title="pending" href="<?php echo base_url(); ?>admin/action-pending-rider/<?php echo $value['rID'];?>"><i class="zmdi zmdi-book"></i></a>
                                        </td>
                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div> <!--End Tab Pending -->
            <?php }elseif($tab_name == 'payments'){?>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Date</th>
                                <th>Rider</th>
                                <th>Email</th>
                                <th>Acc. No</th>
                                <th>Total Earning</th>
                                <th>B.T Commision</th>
                                <th>Rider Earning</th>
                            </tr>
                            </thead>
                             <tbody>
                            <?php if (count($get_payments) != 0): ?>
                                <?php foreach ($get_payments as $value): ?>
                                    <tr>
                                        <td><?php echo $value['uID'] ?></td>
                                        <td><?php echo $value['uJoinDate'];?></td>
                                        <td><?php echo $value['uName'];?></td>
                                        <td><?php echo $value['uEmail'];?></td>
                                        <td><?php echo $value['account_no'];?> </td>
                                        <td style="color:#0080ff; font-weight: bold"><?php echo $value['total_earning'];?>
                                        <td style="color:#ff8000; font-weight: bold"><?php echo $value['bt_commision'];?>
                                        <td style="color:#2dd256; font-weight: bold"><?php echo $value['rider_earning'];?>
                                        </td>
                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
            <?php }elseif($tab_name == 'ride_history'){?>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Date</th>
                                <th>Passenger</th>
                                <th>Rider</th>
                                <th>Fare</th>
                                <th>Distance</th>
                                <th>Ride Rate</th>
                            </tr>
                            </thead>
                             <tbody>
                                <?php if (count($ride_history) != 0): ?>
                                <?php foreach ($ride_history as $value): ?>
                                    <tr>
                                        <td><?php echo $value['uID'] ?></td>
                                        <td><?php if($value['uJoinDate']){ echo $value['uJoinDate']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['passenger']){ echo $value['passenger']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['rider']){ echo $value['rider']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['Fare']){ echo $value['Fare']; }else{ echo '-'; }?> </td>
                                        <td><?php if($value['Distance']){ echo $value['Distance'];} else{echo '-';}?>
                                        <td><?php if($value['rate']){ echo $value['rate'];} else{echo '-';}?>
                                        </td>
                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
            <?php }else{?>
                <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Joined Date</th>
                                <th>Rating</th>
                                <th>Type</th>
                                <th class="display-none-div action-th">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (count($all_riders) != 0): ?>
                                <?php foreach ($all_riders as $value):
                                if($value['rInExRiders'] == 'Internal'){ // Internal
                                    $status = '<span class="btn btn waves-effect waves-light btn-circle" style="background-color:#00FA9A"></span>';
                                }
                                if($value['rInExRiders'] == 'External'){ // Pending
                                    $status = '<span class="btn btn waves-effect waves-light btn-circle" style="background-color:#CD5C5C"></span>';
                                }
                                
                                ?>
                                    <tr>
                                        <td><?php echo $value['rID'] ?></td>
                                        <td><?php if($value['rName']){ echo $value['rName']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['rPhone']){ echo $value['rPhone']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['rJoinDate']){ echo $value['rJoinDate']; }else{ echo '-'; }?></td>
                                        <td><?php if($value['rRatings']){ echo $value['rRatings']; }else{ echo '-'; }?> </td>
                                        <td><?php if($value['rInExRiders']){ echo $status.' '.$value['rInExRiders'];} else{echo '-';}?>
                                        </td>
                                        <td class="tbl-action">
                                            <a href="<?php echo base_url(); ?>admin/edit-rider/<?php echo $value['rID'];?>"><i class="zmdi zmdi-edit"></i></a>
                                        </td>

                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>

            <div class="tab-pane fade" id="ride_history" role="tabpanel"
                 aria-labelledby="rider-history-tab">
                <p>Ride History</p>
            </div>
        </div>
        </div> <!-- end row -->
        <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->
</div>
<!-- End content-page -->

<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div>
<!-- END wrapper -->
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>
<!-- END wrapper -->
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>

<script>
    var resizefunc = [];
</script>

<!-- Responsive examples -->


<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();

        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'colvis']
        });

        table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );
    $("#datesearch").on('change', function(){
        $("#sort-submits-category").submit();
    })
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(function () {
        $('.input-daterange input').each(function() {
            $(this).datepicker({
                autoclose: true,
                todayHighlight: true,
                format:'yyyy-mm-dd' 
            });
        });
    });
</script>

