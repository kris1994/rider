<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <?php
            $segment = $this->uri->segment(3);
            if($segment == null) {
                $table_num_rows = 0;
                $showing = 1;
            } else if($segment == 1) {
                $table_num_rows = 0;
                $showing = 1;
            }elseif($segment > 1){
                $pgs= $segment - 1;
                $table_num_rows = 10 * $pgs;
                $showing = 10 * $pgs;
            }
            ?>
            <!-- end row -->
            <section class="content-header">
                <ul class="list-inline menu-left mb-0">
                    <form autocomplete="off" id="search-all-form" method="post" action="<?= base_url() ?>admin/search-user/1">
                    <li class="hidden-mobile app-search">
                        <div class="input-group custom-input-group">
                            <input type="hidden" name="search_param" value="name" id="search_param">
                            <input name="search" autocomplete="off" id="search-user" class="form-control" placeholder="Search User">
                            <button type="submit" class="button-insider-search"><i class="fa fa-search"></i></button>
                        </div>
                    </li>
                    </form>
                </ul>
                <ol class="breadcrumb">
                    <div class="col-sm-8">
                <form id="sort-submits" method="post" action="<?php echo base_url() ?>admin/passenger">
                    <div class="input-group input-daterange ">
                        <span class="input-group-addon daterange-div text-sm-dt">Date Joined</span>
                        <input id="startDate1" name="startDate1" type="text" value="<?php echo $fromdate?>" class="form-control dateranger" readonly="readonly" placeholder="From"> <span class="input-group-addon"> <span class="zmdi zmdi-calendar color-change"></span></span>
                        <input id="endDate1" name="endDate1" type="text" value="<?php echo $enddate?>" class="form-control dateranger" readonly="readonly" placeholder="To">
                        <span class="input-group-addon daterange-span-radius"> <span class="zmdi zmdi-calendar color-change"></span> </span>
                        <button type="submit" name="btn_search_date"><i class="fa fa-search"></i></button>
                    </div>
                </form>
                    </div>
                </ol>
            </section>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr class="div-bottom-line">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Join Date</th>
                                <th>Rating</th>
                                <th class="display-none-div action-th">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (count($list_passenger) != 0): ?>
                                <?php foreach ($list_passenger as $value):
                                ?>
                                    <tr>
                                        <td><?php echo $value['uID'] ?></td>
                                        <td><?php if($value['uName']){ echo $value['uName']; }else{ echo '-'; }?> </td>
                                        <td><?php if($value['uPhone']){ echo $value['uPhone']; }else{ echo '-'; }?> </td>
                                        <td><?php if($value['uJoinDate']){ echo $value['uJoinDate']; }else{ echo '-'; }?> </td>
                                        <td><?php if($value['rating']){ echo $value['rating']; }else{ echo '-'; }?></td>
                                        <td class="tbl-action">
                                            <a title="view-passenger" href="<?php echo base_url(); ?>admin/view-passenger/<?php echo $value['uID'];?>"><i class="zmdi zmdi-book"></i></a>
                                        </td>
                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end row -->
    </div> <!-- container -->
</div> <!-- content -->
</div>
<!-- End content-page -->
</div>
<!-- END wrapper -->
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>
<!-- END wrapper -->
<?php if($total_db_rows >10){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 10,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>

<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();

        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'colvis']
        });

        table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(function () {
        $('.input-daterange input').each(function() {
            $(this).datepicker({
                autoclose: true,
                todayHighlight: true,
                format:'yyyy-mm-dd' 
            });
        });
    });
</script>
</body>
</html>