<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Admin Users
            <small>All Admin Users</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>admin/create-user"><button type="button" class="btn btn-primary" >Add New</button></a></li>
        </ol>
    </section>
    <?php
    $segment = $this->uri->segment(3);
    if($segment == null) {
        $table_num_rows = 0;
        $showing = 1;
    } else if($segment == 1) {
        $table_num_rows = 0;
        $showing = 1;
    }elseif($segment > 1){
        $pgs= $segment - 1;
        $table_num_rows = 50 * $pgs;
        $showing = 50 * $pgs;
    }
    ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Admin Users</h3>

                        <div class="row">

                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">

                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Email</th>
                                <th>Name</th>
                                <th>Admin Type</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (count($all_users) != 0): ?>
                                <?php foreach ($all_users as $value): ?>

                                    <tr>
                                        <td>
                                            <?php echo $value['uID'] ?>
                                        </td>

                                        <td>
                                            <?php
                                            if($value['uEmail']){
                                                echo $value['uEmail'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['uName']){
                                                echo $value['uName'];
                                            }else{
                                                echo '-';
                                            }?>
                                        </td>
                                        <td>
                                            <?php
                                            if($value['utID']==1){
                                                echo 'Supper Admin';
                                            }elseif($value['utID']==2){
                                                echo 'Admin';

                                            }elseif($value['utID']==4){
                                                echo 'Manager';

                                            }?>
                                        </td>
                                        <td><?php
                                            if($value['uStatus']==0){
                                                echo 'Inactive';
                                            }elseif($value['uStatus']==1){
                                                echo 'Active';
                                            }?>

                                        </td>
                                        <td style="text-align: center">
                                            <?php
                                            if($value['utID'] !=1){
                                                if($value['uStatus']==0){ ?>
                                                    <button data-id="<?= $value['uID'] ?>" class="btn btn-primary btn-xs action-activate">Activate</button>
                                            <?php }elseif($value['uStatus']==1){ ?>
                                                    <button data-id="<?= $value['uID'] ?>" class="btn btn-danger btn-xs action-deactivate">Deactivate</button>
                                            <?php }
                                            }?>
                                            <a href="<?= base_url()?>admin/edit-user/<?= $value['uID'] ?>"><button class="btn btn-success btn-xs">Edit</button></a>
                                        </td>
                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-md-3">
                                <?php

                                if(!$detail_show) {
                                    echo "Showing $showing to $table_num_rows of $total_db_rows entries";
                                }
                                ?>
                            </div>
                            <div class="col-md-9">
                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                    <?php echo $pagination_links; ?>
                                </div>
                            </div>
                        </div>
                        <!-- /pagination and status -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<form id="activate-admin" method="post" action="<?= base_url()?>admin/activateAdmin">
    <input id="activate-id" type="hidden" name="user" value="">
</form>

<form id="deactivate-admin" method="post" action="<?= base_url()?>admin/deactivateAdmin">
    <input id="deactivate-id" type="hidden" name="user" value="">
</form>

<?php if($this->session->flashdata('activate_user') == "error"): ?>
    <script type="text/javascript">
        sweetAlert("Oops...", "Error on Activate!", "error");
    </script>
<?php endif; ?>
<?php if($this->session->flashdata('activate_user') == "done"): ?>
    <script type="text/javascript">
        sweetAlert("Success", "Successful!", "success");
    </script>
<?php endif; ?>

<?php if($this->session->flashdata('deactivate_user') == "error"): ?>
    <script type="text/javascript">
        sweetAlert("Oops...", "Error on Deactivate!", "error");
    </script>
<?php endif; ?>
<?php if($this->session->flashdata('deactivate_user') == "done"): ?>
    <script type="text/javascript">
        sweetAlert("Success", "Successful!", "success");
    </script>
<?php endif; ?>

<script>
    $(document).on('click','.action-activate', function () {
        var id= $(this).attr('data-id');
        $("#activate-id").val(id);
        swal({
                title: "Are you sure?",
                text: "You want to activate this user?",
                type: "warning",   showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $('#activate-admin').trigger('submit');
                }
            });
    });

    $(document).on('click','.action-deactivate', function () {
        var id= $(this).attr('data-id');
        $("#deactivate-id").val(id);
        swal({
                title: "Are you sure?",
                text: "You want to deactivate this user?",
                type: "warning",   showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $('#deactivate-admin').trigger('submit');
                }
            });
    });
</script>



<?php if($total_db_rows >50){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 50,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);

        });

    </script>
<?php } ?>

<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            endDate: new Date(),
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: 'yyyy-mm-dd'
        });
        $('#sort-by-type').on('change', function(){
            var val = $('#sort-by-type option:selected').text();

            $('form#sort-submit').submit();
        });

        $("#search-location").on('keyup', function () {
            var location = $('#search-location').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/searchAutoComplete",
                data: {location: location},
                success: function (res) {
                    $("#searchResult").html(res);
                    $("#searchResult").slideDown();
                }
            });
        });
        $('#searchResult').on('click', '.search-text',function () {
            var searchKey = $(this).attr("data-value");
            $("#search-location").val(searchKey);
            $('form#search-all-form').trigger('submit');

        });
    });
</script>