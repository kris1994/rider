
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40">
                                <form autocomplete="on" id="form-send-location" method="post" action="<?= base_url() ?>admin/addLocation1" enctype="multipart/form-data">

                                    <div class="row">
                                        <div class="col-6">
                                            <fieldset disabled>
                                                <div class="form-group">
                                                    <label for="disabledTextInput">Biker ID</label>
                                                    <input type="text" id="disabledTextInput" class="form-control" placeholder="ABC - 1234" >
                                                </div>

                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="TextInput3">Vehicle Number</label>
                                                    <input type="text" class="form-control" id="TextInput3" placeholder="BCK 1245" name="vehicle_number" required>

                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="TextInput">Brand</label>
                                                <input type="text" class="form-control" id="TextInput" placeholder="Honda" name="brand" required >
                                            </fieldset>
                                        </div>
                                        <!-- <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="TextInput4">Chassis Number</label>
                                                <input type="text" class="form-control" id="TextInput4" placeholder="1DGCM25899S564875" name="chassis_number" required>

                                            </fieldset>
                                        </div> -->
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="TextInput2">Type</label>
                                                <input type="text" class="form-control" id="TextInput2" placeholder="Navi" name="type" required>
                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset>
                                                <label for="TextInput4">Insurance Expiry</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" name="insurance_exp" id="datepicker-autoclose" required>
                                                    <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                </div><!-- input-group -->
                                            </fieldset>

                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="exampleSelect1" id="laberl_ass_rider">Assign Rider</label>
                                                <select name="internal-rider" class="form-control" id="exampleSelect1">
                                                        <option value="">Select</option>
                                                    <?php foreach ($internal_riders as $row ): ?>
                                                        <option value="<?= $row['rName'] ?>"><?= $row['rName'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-6">
                                            <fieldset class="form-group">
                                                <label for="exampleSelect1" id="laberl_ass_rider">Vehicles</label>
                                                <select name="vehicles" class="form-control" id="exampleSelect1">
                                                        <option value="">Select</option>
                                                    <?php foreach ($price_master_details as $row ): ?>
                                                        <option value="<?php echo $row['vehicle_cat'] ?>"><?php echo $row['vehicle_cat'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </fieldset>
                                        </div>

                                    </div>

                                    <div class="form-group text-right">
                                        <div class="col-12">
                                            <button class="btn btn-cancel  waves-effect waves-light" type="reset">
                                                Cancel
                                            </button>
                                            <button id="add-loc-btn" class="btn  btn-send btn btn-save waves-effect m-l-5" type="submit">Save</button>
<!--                                        <button type="reset" class="btn btn-save waves-effect m-l-5">-->
<!--                                            Save-->
<!--                                        </button>-->
                                        </div>
                                    </div>
                                </form>

                            </div><!-- end col -->

                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</div>
<!-- End content-page -->
<script>
    var resizefunc = [];
</script>