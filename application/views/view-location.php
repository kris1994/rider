<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Locations
            <small>View Location</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>admin/edit-location/<?= $location[0]['lID'] ?>"><button type="button" class="btn btn-primary" >Edit</button></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-map-marker"></i> Location Info </h2>

                                    <div class="clearfix"></div>
                                </div>
                                <hr />
                                <div class="x_content">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Location Name - English</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lName']){
                                                    echo $location[0]['lName'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Location Name - Sinhala</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if(count($location_sinhala) >0){
                                                    if($location_sinhala[0]['lName']){
                                                        echo $location_sinhala[0]['lName'];
                                                    }else{
                                                        echo '-';
                                                    }

                                                } else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Location Name - Tamil</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if(count($location_tamil) >0){
                                                    if($location_tamil[0]['lName']){
                                                        echo $location_tamil[0]['lName'];
                                                    }else{
                                                        echo '-';
                                                    }

                                                } else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>

                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Location Owner / Added By</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['owner']){
                                                    echo $location[0]['owner'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Location Area</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p><?php if($location[0]['lArea']){
                                                    echo $location[0]['lArea'];
                                                }else{
                                                    echo '-';
                                                } ?></p>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Parking Type</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p><?php if($location[0]['pkID'] == 1 ){
                                                    echo "Private";
                                                }elseif($location[0]['pkID'] == 2 ){
                                                    echo "Street";
                                                }elseif($location[0]['pkID'] == 3 ){
                                                    echo "TPS";
                                                } ?></p>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Landmark - English</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lLandmark']){
                                                    echo $location[0]['lLandmark'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Landmark - Sinhala</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if(count($location_sinhala) >0){
                                                    if($location_sinhala[0]['lLandmark']){
                                                        echo $location_sinhala[0]['lLandmark'];
                                                    }else{
                                                        echo '-';
                                                    }

                                                } else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Landmark - Tamil</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if(count($location_tamil) >0){
                                                    if($location_tamil[0]['lLandmark']){
                                                        echo $location_tamil[0]['lLandmark'];
                                                    }else{
                                                        echo '-';
                                                    }

                                                } else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Address - English</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lAddress']){
                                                    echo $location[0]['lAddress'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Address - Sinhala</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if(count($location_sinhala) >0){
                                                    if($location_sinhala[0]['lAddress']){
                                                        echo $location_sinhala[0]['lAddress'];
                                                    }else{
                                                        echo '-';
                                                    }

                                                } else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Address - Tamil</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if(count($location_tamil) >0){
                                                    if($location_tamil[0]['lAddress']){
                                                        echo $location_tamil[0]['lAddress'];
                                                    }else{
                                                        echo '-';
                                                    }

                                                } else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Special Notes - English</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <p>
                                                <?php if($location[0]['lAbout']){
                                                    echo nl2br($location[0]['lAbout']);
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Special Notes - Sinhala</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <p>
                                                <?php if(count($location_sinhala) >0){
                                                    if($location_sinhala[0]['lAbout']){
                                                        echo nl2br($location_sinhala[0]['lAbout']);
                                                    }else{
                                                        echo '-';
                                                    }

                                                } else{
                                                    echo '-';
                                                } ?>


                                            </p>
                                        </div>
                                    </div>
                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Special Notes - Tamil</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <p>
                                                <?php if(count($location_tamil) >0){
                                                    if($location_tamil[0]['lAbout']){
                                                        echo nl2br($location_tamil[0]['lAbout']);
                                                    }else{
                                                        echo '-';
                                                    }
                                                } else{
                                                    echo '-';
                                                } ?>


                                            </p>
                                        </div>
                                    </div>

                                    <hr />

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>URL</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lUrl']){
                                                    echo $location[0]['lUrl'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Latitude</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lLatitude']){
                                                    echo $location[0]['lLatitude'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Longitude</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lLongitude']){
                                                    echo $location[0]['lLongitude'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <?php if($location[0]['pkID'] == 3): ?>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Start Latitude</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lStartLatitude']){
                                                    echo $location[0]['lStartLatitude'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Start Longitude</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lStartLongitude']){
                                                    echo $location[0]['lStartLongitude'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>End Latitude</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lEndLatitude']){
                                                    echo $location[0]['lEndLatitude'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>End Longitude</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lEndLongitude']){
                                                    echo $location[0]['lEndLongitude'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <?php endif; ?>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Location Tags</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <?php foreach ($tags as $tag): ?>
                                            <p>
                                                <?= $tag['ltTag']?>
                                            </p>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Currency</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lCurrency']){
                                                    echo $location[0]['lCurrency'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Status</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lStatus']==0){
                                                    echo 'Draft';
                                                }elseif($location[0]['lStatus']==1){
                                                    echo 'Available';
                                                }elseif($location[0]['lStatus']==2){
                                                    echo 'Not Available';
                                                } elseif($location[0]['lStatus']==3){
                                                    echo 'On Vacation';
                                                }?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Added</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lAdded']){
                                                    echo $location[0]['lAdded'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 location-div">
                                            <p>Is Approved</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <p> <?php if($location[0]['lIsApproved']==1){
                                                    echo 'Approved';
                                                }else{
                                                    echo 'Not Approved';
                                                } ?></p>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Approved Date</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['lApproved']){
                                                    echo $location[0]['lApproved'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Approved By</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['approved_by']){
                                                    echo $location[0]['approved_by'];
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Is Contributed</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['IsContributed']==1){
                                                    echo 'Yes';
                                                }else{
                                                    echo 'No';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Contributor</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8 location-div">
                                            <p>
                                                <?php if($location[0]['IsContributed']==1){
                                                    if($location[0]['lContributor'] > 0){
                                                        echo $location[0]['contributor'];
                                                    }else{
                                                        echo 'Anonymous Contributor';
                                                    }
                                                }else{
                                                    echo '-';
                                                } ?>
                                            </p>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row ">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Opening Hour - English</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <?php if(count($opening_hour) >0){?>
                                            <div class="col-sm-2">Monday</div>
                                            <div class="col-sm-4 location-div"><?= $opening_hour[0]['ohMon'] ?></div>
                                            <div class="col-sm-2">Tuesday</div>
                                            <div class="col-sm-4 location-div"><?= $opening_hour[0]['ohTue'] ?></div>
                                            <div class="col-sm-2">Wednesday</div>
                                            <div class="col-sm-4 location-div"><?= $opening_hour[0]['ohWed'] ?></div>
                                            <div class="col-sm-2">Thursday</div>
                                            <div class="col-sm-4 location-div"><?= $opening_hour[0]['ohThu'] ?></div>
                                            <div class="col-sm-2">Friday</div>
                                            <div class="col-sm-4 location-div"><?= $opening_hour[0]['ohFri'] ?></div>
                                            <div class="col-sm-2">Saturday</div>
                                            <div class="col-sm-4 location-div"><?= $opening_hour[0]['ohSat'] ?></div>
                                            <div class="col-sm-2">Sunday</div>
                                            <div class="col-sm-4 location-div"><?= $opening_hour[0]['ohSun'] ?></div>
                                            <?php }else{
                                                echo '-';
                                            }?>
                                        </div>
                                    </div>

                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Opening Hour - Sinhala</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <?php if(count($opening_hour_sinhala) >0){?>
                                                <div class="col-sm-2">Monday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_sinhala[0]['ohMon'] ?></div>
                                                <div class="col-sm-2">Tuesday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_sinhala[0]['ohTue'] ?></div>
                                                <div class="col-sm-2">Wednesday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_sinhala[0]['ohWed'] ?></div>
                                                <div class="col-sm-2">Thursday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_sinhala[0]['ohThu'] ?></div>
                                                <div class="col-sm-2">Friday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_sinhala[0]['ohFri'] ?></div>
                                                <div class="col-sm-2">Saturday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_sinhala[0]['ohSat'] ?></div>
                                                <div class="col-sm-2">Sunday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_sinhala[0]['ohSun'] ?></div>
                                            <?php }else{
                                                echo '-';
                                            }?>
                                        </div>
                                    </div>

                                    <div class="row lang-space">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Opening Hour - Tamil</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <?php if(count($opening_hour_tamil) >0){?>
                                                <div class="col-sm-2">Monday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_tamil[0]['ohMon'] ?></div>
                                                <div class="col-sm-2">Tuesday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_tamil[0]['ohTue'] ?></div>
                                                <div class="col-sm-2">Wednesday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_tamil[0]['ohWed'] ?></div>
                                                <div class="col-sm-2">Thursday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_tamil[0]['ohThu'] ?></div>
                                                <div class="col-sm-2">Friday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_tamil[0]['ohFri'] ?></div>
                                                <div class="col-sm-2">Saturday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_tamil[0]['ohSat'] ?></div>
                                                <div class="col-sm-2">Sunday</div>
                                                <div class="col-sm-4 location-div"><?= $opening_hour_tamil[0]['ohSun'] ?></div>
                                            <?php }else{
                                                echo '-';
                                            }?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Available Facility</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <?php if(count($location_facility) >0){?>
                                            <?php foreach ($location_facility as $row): ?>
                                                <div class="col-sm-4 location-div"><?= $row['fName'] ?></div>
                                            <?php endforeach; ?>
                                            <?php }else{
                                                echo '-';
                                            }?>

                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Location Images</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <?php if(count($location_img) >0){?>
                                            <?php foreach ($location_img as $row): ?>
                                                <div class="col-sm-6" style="padding-bottom: 20px">
                                                    <img width="100%" height="175" src="<?= base_url() ?>uploads/location_img/<?= $row['liImage'] ?>" >
                                                </div>
                                            <?php endforeach; ?>
                                            <?php }else{
                                                echo '-';
                                            }?>

                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <p>Street View Image</p>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <?php if($location[0]['lUrl']){
                                                $st_val = explode(',', $location[0]['lUrl']);
                                                $lat_val = explode('@', $st_val[0]);
                                                $lat = $lat_val[1];
                                                $long = $st_val[1];
                                                $heads = explode('h', $st_val[4]);
                                                $head = $heads[0];
                                                ?>
                                                <img class="street-view-img" width="100%" src="https://maps.googleapis.com/maps/api/streetview?size=640x360&location=<?php echo $lat; ?>,<?php echo $long; ?>&heading=<?php echo $head; ?>&key=AIzaSyCYNlFJ_Hhz7CIot7JeSEZwfwhhOfedUPE">
                                            <?php  }?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div id="antoox">
                                        <div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>

                                    </div>
                                    <?php if($location[0]['lIsApproved']==1){ ?>
                                        <div class="btn btn-primary btn-location">Approved</div>
                                        <?php if($location[0]['lStatus']!=5){ ?>
                                        <form method="post" action="<?= base_url()?>admin/deactivate">
                                            <input type="hidden" name="location" value="<?= $location[0]['lID']; ?>">
                                            <button class="btn btn-danger delete-location" type="submit">Deactivate</button>
                                        </form>
                                        <?php }else{ ?>
                                            <div class="btn btn-success delete-location">Deactivated by Owner</div>

                                        <?php } ?>
                                    <?php }else{ ?>
                                    <?php if($location[0]['lStatus'] == 5){ ?>
                                        <form method="post" action="<?= base_url()?>admin/giveApprovalSpot">
                                        <?php }else{ ?>
                                            <form method="post" action="<?= base_url()?>admin/giveApproval">
                                                <?php } ?>
                                            <input type="hidden" name="location" value="<?= $location[0]['lID']; ?>">
                                        <button class="btn btn-primary btn-location" type="submit">Give Approval</button>
                                        </form>

                                    <?php if($location[0]['lStatus']==4 && $location[0]['lStatus']!=5){ ?>
                                        <form method="post" action="<?= base_url()?>admin/activateLocation">
                                            <input type="hidden" name="location" value="<?= $location[0]['lID']; ?>">
                                            <button class="btn btn-success delete-location" type="submit">Activate</button>
                                        </form>
                                        <?php } ?>
                                        <?php if($location[0]['lStatus']!=4 && $location[0]['lStatus']!=5){ ?>
                                        <form method="post" action="<?= base_url()?>admin/deactivate">
                                            <input type="hidden" name="location" value="<?= $location[0]['lID']; ?>">
                                            <button class="btn btn-danger delete-location" type="submit">Deactivate</button>
                                        </form>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>



