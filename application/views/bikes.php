<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <?php
            $segment = $this->uri->segment(3);
            if($segment == null) {
                $table_num_rows = 0;
                $showing = 1;
            } else if($segment == 1) {
                $table_num_rows = 0;
                $showing = 1;
            }elseif($segment > 1){
                $pgs= $segment - 1;
                $table_num_rows = 10 * $pgs;
                $showing = 10 * $pgs;
            }
            ?>
            <!-- end row -->
            <section class="content-header">
                <ul class="list-inline menu-left mb-0">
                    <form autocomplete="off" id="search-all-form" method="post" action="<?= base_url() ?>admin/search-bike/1">
                        <li class="hidden-mobile app-search">
                            <div class="input-group custom-input-group">
                                <input type="hidden" name="search_param" value="name" id="search_param">
                                <input name="search" autocomplete="off" id="search-user" class="form-control" placeholder="Search User">
                                <button type="submit" class="button-insider-search"><i class="fa fa-search"></i></button>
                            </div>
                        </li>
                    </form>
                </ul>

                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url()?>admin/addRider"><button type="button" class="btn btn-addnew " ><i class="zmdi zmdi-bike"></i>Add New</button></a></li>
                </ol>
            </section>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive table-margin">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Brand</th>
                                <th>Type</th>
                                <th>Vehicle Number</th>
                                <th>Insurance Exp.</th>
                                <th>Rider</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if (count($all_bikes) != 0): ?>
                                <?php foreach ($all_bikes as $value): ?>
                                    <tr>
                                        <td><?php echo $value['bID'] ?></td>
                                        <td><?php if($value['bBrand']){ echo $value['bBrand']; }else{ echo '-'; }?> </td>
                                        <td><?php if($value['bType']){echo $value['bType'];}else{echo '-';}?></td>
                                        <td><?php if($value['bChassisNumber']){ echo $value['bChassisNumber']; }else{ echo '-'; }?> </td>
                                        <td><?php if($value['bVehicleNumber']){echo $value['bVehicleNumber'];}else{echo '-';}?></td>
                                        <td><?php if($value['bAssignRider']){ echo $value['bAssignRider']; }else{ echo '-'; }?> </td>
                                    </tr>
                                    <?php $table_num_rows++; ?>
                                <?php endforeach ?>
                                <?php
                                $detail_show = false;
                                ?>
                            <?php else: ?>
                                <?php
                                $detail_show = true;
                                ?>
                            <?php endif; ?>
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-md-3">
                                <?php
                                if(!$detail_show) {
                                    echo "Showing $showing to $table_num_rows of $total_db_rows entries";
                                }
                                ?>
                            </div>
                            <div class="col-md-9">
                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                    <?php echo $pagination_links; ?>
                                </div>
                            </div>
                        </div>
                        <!-- /pagination and status -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <?php if($total_db_rows >50){ ?>
        <script>
            $(function() {
                $('#example2_paginate').pagination({
                    items: <?= $total_db_rows ?>,
                    itemsOnPage: 50,
                    cssStyle: 'compact-theme',
                    hrefTextPrefix: '',
                    displayedPages: 3

                });
                $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
            });
        </script>
    <?php } ?>
    <script>
        var resizefunc = [];
    </script>
