<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <?php
            $segment = $this->uri->segment(3);
            if($segment == null) {
                $table_num_rows = 0;
                $showing = 1;
            } else if($segment == 1) {
                $table_num_rows = 0;
                $showing = 1;
            }elseif($segment > 1){
                $pgs= $segment - 1;
                $table_num_rows = 10 * $pgs;
                $showing = 10 * $pgs;
            }
            ?>
            <!-- end row -->


            <section class="content-header">
                <ul class="list-inline menu-left mb-0">
                    <form autocomplete="off" id="search-all-form" method="post" action="<?= base_url() ?>admin/search-user/1">
                        <li class="hidden-mobile app-search">
                            <div class="input-group custom-input-group">
                                <input type="hidden" name="search_param" value="name" id="search_param">
                                <input name="search" autocomplete="off" id="search-user" class="form-control" placeholder="Search User">
                                <button type="submit" class="button-insider-search"><i class="fa fa-search"></i></button>
                            </div>
                        </li>
                    </form>
                </ul>

                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url()?>admin/addRider"><button type="button" class="btn btn-addnew " ><img class="btn-bike-img"  src="<?php echo base_url(); ?>assets/images/btn-bike.png">Add New</button></a></li>
                </ol>
            </section>

                    <div class="row">
                        <div class="col-12">
                            <div class="card-box table-responsive table-margin">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Contact Number</th>
                                        <th>Joined Date</th>
                                        <th>Rating</th>
                                        <th>Type</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                        <?php if (count($all_users) != 0): ?>
                                            <?php foreach ($all_users as $value): ?>
                                                <tr>
                                                    <td><?php echo $value['rID'] ?></td>
                                                    <td><?php if($value['rName']){ echo $value['rName']; }else{ echo '-'; }?></td>
                                                    <td><?php if($value['rPhone']){ echo $value['rPhone']; }else{ echo '-'; }?></td>
                                                    <td><?php if($value['rJoinDate']){ echo $value['rJoinDate']; }else{ echo '-'; }?></td>
                                                    <td><?php if($value['rRatings']){ echo $value['rRatings']; }else{ echo '-'; }?> </td>
                                                    <td><?php if($value['rInExRiders']){ echo $value['rInExRiders']; }else{ echo '-'; }?> </td>
                                                </tr>
                                                <?php $table_num_rows++; ?>
                                            <?php endforeach ?>
                                            <?php
                                            $detail_show = false;
                                            ?>
                                        <?php else: ?>
                                            <?php
                                            $detail_show = true;
                                            ?>
                                        <?php endif; ?>
                                    </tbody>
                        </table>

                        <div class="row">
                            <div class="col-md-3">
                                <?php
                                if(!$detail_show) {
                                    echo "Showing $showing to $table_num_rows of $total_db_rows entries";
                                }
                                ?>
                            </div>
                            <div class="col-md-9">
                                <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                    <?php echo $pagination_links; ?>
                                </div>
                            </div>
                        </div>
                        <!-- /pagination and status -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php if($total_db_rows >50){ ?>
    <script>
        $(function() {
            $('#example2_paginate').pagination({
                items: <?= $total_db_rows ?>,
                itemsOnPage: 50,
                cssStyle: 'compact-theme',
                hrefTextPrefix: '',
                displayedPages: 3

            });
            $('#example2_paginate').pagination('drawPage', <?= $segment ?>);
        });
    </script>
<?php } ?>



    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.js"></script>

    <!-- Required datatable js -->
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- App js -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            endDate: new Date(),
            weekStart: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: 'yyyy-mm-dd'
        });
        $('#sort-user').on('change', function(){
            var val = $(this).val();
            $('form#sort-submits').submit();
        });
        $('#sort-by-active').on('change', function(){
            var val = $(this).val();
            $('form#sort-submit-active').submit();
        });


        $("#search-user").on('keyup', function () {
            var user = $('#search-user').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "admin/searchUserAutoComplete",
                data: {user: user},
                success: function (res) {
                    $("#searchResult").html(res);
                    $("#searchResult").slideDown();
                }
            });
        });
        $('#searchResult').on('click', '.search-text',function () {
            var searchKey = $(this).attr("data-value");
            $("#search-user").val(searchKey);
            $('form#search-all-form').trigger('submit');

        });

    });
</script>

