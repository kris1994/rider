<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box no-padding">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40 no-padding">

                                <form autocomplete="on" id="form-send-location" method="post" action="<?php echo base_url() ?>admin/addPromocode" enctype="multipart/form-data">
                                <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-3">
                                                <fieldset class="form-group">
                                                        <labelfor="disabledTextInput"  style="color:black">Discount Type</label>
                                                         <div class="radio radio-warning radio-circle">
                                                            <input id="radio-12" type="radio" name="discount_type" value="Percentage">
                                                            <label class="col-lg-4" for="radio-12"> Percentage</label>
                                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                             <input id="radio-13" type="radio" name="discount_type" value="Amount">
                                                            <label class="col-lg-4" for="radio-13"> Amount</label>
                                                        </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                            <div class="col-3">
                                                <fieldset class="form-group">
                                                    <label for="discount">Discount Value</label>
                                                    <input type="text" class="form-control" id="discount" placeholder="20%" name="discount" >
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Start Date</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="start_date" id="datepicker-autoclose" >
                                                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <fieldset class="form-group">
                                                    <label for="discount">Valid Number Of Riders</label>
                                                    <input type="text" class="form-control" id="discount" placeholder="10" name="discount" >
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                             <div class="col-3">
                                                <fieldset class="form-group">
                                                    <label for="promo_code">Promo Code</label>
                                                    <input type="text" class="form-control" id="promo_code" placeholder="Promo Code 001" name="promo_code" >
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Expiration</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="expiration" id="datepicker-autoclose" >
                                                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                             <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label style="color:black" for="TextInput4"><b>Message</b></label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-7">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Type Your Message</label>
                                                    <div class="input-group">
                                                       <textarea name="message" class="form-control" rows="4"></textarea>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label style="color:black" for="TextInput4"><b>Recipients</b></label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-3">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Select Filter Type</label>
                                                    <div class="input-group">
                                                       <select name="recipients" class="form-control">
                                                           <option value="Joined Date">Joined Date</option>
                                                           <option value="Ride Total">Ride Total</option>
                                                           <option value="Number of Rides">Number of Rides</option>
                                                           <option value="Rating">Rating</option>
                                                       </select>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                            <div class="col-2">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Ride Total(LKR)</label>
                                                    <div class="input-group">
                                                       <input type="text" name="ride_total" class="form-control" value="0">
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                            <div class="col-1">
                                                <label for="TextInput4">&nbsp;</label>
                                            </div>
                                             <div class="col-2">
                                                <fieldset class="form-group">
                                                    <label for="TextInput">All Passenger</label><br>
                                                    <label class="switch">
                                                        <input class="switch-input" type="checkbox" name="all_passenger" />
                                                        <span class="switch-label" data-on="On" data-off="Off"></span>
                                                        <span class="switch-handle"></span>
                                                    </label>

                                                    <!-- <input type="hidden" id="type" name="rInExRiders" value="External"> -->
                                                </fieldset>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">&nbsp;</label>
                                                </fieldset>
                                            </div>
                                             <div class="col-4">
                                                <fieldset class="form-group">
                                                    <div class="input-group input-daterange " style="border:1px solid #ccc;">
                                                    <input id="startDate1" name="startDate1" type="text" class="form-control dateranger" readonly="readonly" placeholder="From" style="text-align: left">
                                                    <span class="input-group-addon">
                                                    <span class="zmdi zmdi-calendar color-change">
                                                    </span></span>
                                                    <input id="endDate1" name="endDate1" type="text" class="form-control dateranger" readonly="readonly" placeholder="To" style="text-align: left">
                                                    <span class="input-group-addon daterange-span-radius">
                                                    <span class="zmdi zmdi-calendar color-change"></span></span>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                          <div class="row pull-right">
                                            <div class="divider"></div>
                                                <div class="form-group text-right m-b-0">
                                                    <a href="<?php echo base_url('admin/promo_codes')?>" class="btn btn-cancel  waves-effect waves-light">Discard</a>
                                                    <button id="add-loc-btn" class="btn  btn-send btn btn-save waves-effect m-l-5" type="submit">Done</button>
                                                </div>
                                         </div>
                                </form>
                            </div><!-- end col -->

                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
</div>
<!-- End content-page -->
</div>
<!-- END wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.input-daterange input').each(function() {
            $(this).datepicker({
                autoclose: true,
                todayHighlight: true,
                format:'dd/mm/yyyy' 
            });
        });
    });
</script>
<script>
$(".datepicker").datepicker({
autoclose: true,
todayHighlight: true,
format:'dd/mm/yyyy' 
});
    var resizefunc = [];
</script>


<script>
$(".datepicker").datepicker({
autoclose: true,
todayHighlight: true,
format:'dd/mm/yyyy' 
});

</script>
<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
</script>
<script type="text/javascript">
function parkingFee() {
if ($('#price').is(':checked')) {
$('#price-details :input').attr('disabled', true);
} else {
$('#price-details :input').removeAttr('disabled');
}
}
</script>


</body>
</html>