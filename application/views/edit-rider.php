<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box no-padding">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40 no-padding">
                                <form autocomplete="on" id="form-send-location" method="post" action="<?= base_url() ?>admin/editRiderProcess" enctype="multipart/form-data">
                                    <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-4">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <input type="hidden" name="hddrID" value="<?php echo $location->rID;?>">
                                                        <label for="disabledTextInput">First Name</label>
                                                        <input type="text" id="disabledTextInput" class="form-control"
                                                               placeholder="Jeanelle" name="rider_name" value="<?php echo @$location->rName;?>">
                                                       
                                                    </div>

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Contact</label>
                                                    <input type="text" class="form-control" id="TextInput3"
                                                           placeholder="079 7979 797" name="contact_no" required value="<?php echo @$location->rPhone?>">                                               
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput">External Rider</label><br>
                                                    <label class="switch">
                                                        <input class="switch-input" type="checkbox" value="1" />
                                                        <span class="switch-label" data-on="<?php echo @$location->rInExRiders?>" data-off="External"></span>
                                                        <span class="switch-handle"></span>
                                                    </label>
                                                        <input type="hidden" id="type" name="rInExRiders" value="External">
                                                </fieldset>
                                            </div>

                                           <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Last Name</label>
                                                    <input type="text" class="form-control" id="TextInput4"
                                                           placeholder="Wanner" name="last_name" required value="<?php echo @$location->rLastName?>">

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput2">Email</label>
                                                    <input type="text" class="form-control" id="TextInput2"
                                                           placeholder="Jeanellewanner@gmail.com" name="rider_email" value="<?php echo $location->rEmail;?>" required>
                                                </fieldset>
                                            </div>
                                            <!-- <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect1">Assign Bike</label>
                                                    <select class="form-control" id="exampleSelect1">
                                                        <?php foreach ($internal_bikes as $row ): ?>
                                                            <option <?php if($row['bVehicleNumber']){echo 'selected';} ?> value="<?php echo  $row['bID'] ?>"><?php echo $row['bVehicleNumber'] ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </fieldset>
                                            </div> -->
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                <label for="exampleSelect1" id="laberl_ass_rider">Vehicle Type</label>
                                                <select name="vehicle_type" class="form-control vehicle_type" id="exampleSelect1">
                                                        <option value="">Select</option>
                                                    <?php foreach ($price_master_details as $row ): ?>
                                                        <option value="<?php echo $row['vehicle_cat'] ?>" <?php echo (@$location->vehicle_type == $row['vehicle_cat']) ? 'selected' : ''?> ><?php echo $row['vehicle_cat']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </fieldset>
                                            </div>
                                            <div class="col-4">
                                                 <fieldset class="form-group">
                                                    <label for="exampleSelect1" id="laberl_ass_rider">Vehicle Number</label>
                                                    <select name="vehicles" class="form-control vehicle_no" id="exampleSelect1">
                                                          
                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="divider"></div>


                                    <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-4">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <label for="disabledTextInput">EPF Number</label>
                                                        <input type="text" class="form-control" id="EPF Number"
                                                           placeholder="" name="rider_epfno" required value="<?php echo @$location->rEpfNumber;?>">
                                                    </div>

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Salary</label>
                                                    <input type="text" class="form-control" id="Salary"
                                                           placeholder="" name="rider_salary" required value="<?php echo @$location->rSalary;?>">
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Phone EMI Number</label>
                                                    <input type="text" class="form-control" id="Number"
                                                           placeholder="" name="phone_emi" required value="<?php echo @$location->rEmiNumber;?>">
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Equipments</label>
                                                    <select  multiple data-role="tagsinput" name="equipments[]">
                                                        <?php
                                                        $equipt = explode(',',$location->rEquipments);
                                                        foreach ($equipt as $e) {
                                                        ?>
                                                        <option value="<?php echo $e?>"></option>
                                                        <?php }?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput2">Initial Deposite</label>
                                                    <input type="text" id="Deposite" class="form-control"
                                                               placeholder="" name="initial_deposit" required value="<?php echo @$location->rInitialDeposite;?>">
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput4">Byker Phone Number</label>
                                                    <input type="text" class="form-control" id="BykerPhone"
                                                           placeholder="" name="byker_phone_number" required value="<?php echo @$location->rBykerPhoneNumber;?>">
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                    <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-12">
                                                <h4 class="header-title m-t-0 m-b-30">Documents</h4>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-4">
                                                <fieldset>

                                                    <?php $ncf = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 1));
                                                    $ncf_file = base_url()."uploads/document_img/".@$ncf->rdImage;
                                                    ?>
                                                    <div class="form-group">
                                                        <label for="disabledTextInput">National Identity Card (Front)</label>
                                                        <input type="file" name="image[]"  class="dropify" data-default-file="<?php echo $ncf_file?>" data-height="172" />
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $ncb = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 2));
                                                    $ncb_file = base_url("uploads/document_img/".@$ncb->rdImage);
                                                    ?>
                                                    <label for="TextInput3">National Identity Card (Back)</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo $ncb_file?>" class="dropify" data-height="172" />

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $bpb = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 3));
                                                    $bpb_file = base_url("uploads/document_img/".@$bpb->rdImage);
                                                    ?>
                                                    <label for="TextInput3">Bank Pass Book</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo $bpb_file?>"  class="dropify" data-height="172" />

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $dlf = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 4));
                                                    $dlf_file = base_url("uploads/document_img/".@$dlf->rdImage);
                                                    ?>
                                                    <label for="TextInput4">Driving License (Front)</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo $dlf_file?>" class="dropify" data-height="172" />
                                                </fieldset>
                                                <fieldset>
                                                    <label for="TextInput4">Insurance Expiry</label>
                                                    <div class="input-group">

                                                        <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="driving_expe[3]" id="datepicker-autoclose" value="<?php echo @$dlf->rdExDate ?>">
                                                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                    </div><!-- input-group -->
                                                </fieldset>

                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $dlb = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 5));
                                                    $dlb_file = base_url("uploads/document_img/".@$dlb->rdImage);
                                                    ?>
                                                    <label for="TextInput2">Driving License (Back)</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo $dlb_file?>" class="dropify" data-height="172" />

                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $rpd = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 6));
                                                    $rpd_file = base_url("uploads/document_img/".@$rpd->rdImage);
                                                    ?>
                                                    <label for="TextInput4" id="rpdchange">Revenue License</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo $rpd_file?>" class="dropify" data-height="172" />
                                                </fieldset>
                                                <fieldset>
                                                    <label for="TextInput4">Revenue Expiry</label>
                                                    <div class="input-group">

                                                        <input type="text" class="form-control datepicker" placeholder="mm/dd/yyyy" name="driving_expe[5]" value="<?php echo @$rpd->rdExDate?>" id="datepicker-autoclose" >
                                                        <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                                                    </div><!-- input-group -->
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <?php $pr = $this->admin_model->GetInfoByRow('rider_document','',array('rID' => $location->rID, 'subID' => 7));
                                                    $pr_file = base_url("uploads/document_img/".@$pr->rdImage);
                                                    ?>
                                                    <label for="TextInput4" id="prgn">Police Report / Grama Niladari Report (Optional)</label>
                                                    <input type="file" name="image[]" data-default-file="<?php echo $pr_file?>" class="dropify" data-height="172" />

                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="form-group text-right m-b-0">
                                            <button class="btn btn-cancel  waves-effect waves-light" type="reset">Cancel</button>
                                            <button id="add-loc-btn" class="btn  btn-send btn btn-save waves-effect m-l-5" type="submit">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- end col -->

                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
</div>
<!-- End content-page -->
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<script>
$(document).ready(function(){
        $.ajax({
            type: "POST",
            url: base_url+"admin/getVehicleNo",
            data: {
                vehicle_type: $(".vehicle_type").val()
            },
            dataType: "json",
            success: function(data) {
                var _html = "";
                jQuery.each(data, function(key, value) {
                    // console.log(value['bVehicleNumber']);
                    _html += "<option value="+value['bVehicleNumber']+">"+value['bVehicleNumber']+"</option>";
                });
                $('.vehicle_no').html(_html);
            }
        });
});
$(".vehicle_type").change(function(){
    $.ajax({
        type: "POST",
        url: base_url+"admin/getVehicleNo",
        data: {
            vehicle_type: $(this).val()
        },
        dataType: "json",
        success: function(data) {
            var _html = "";
            jQuery.each(data, function(key, value) {
                // console.log(value['bVehicleNumber']);
                _html += "<option value="+value['bVehicleNumber']+">"+value['bVehicleNumber']+"</option>";
            });
            $('.vehicle_no').html(_html);
        }
    });
})
$(".datepicker").datepicker({
autoclose: true,
todayHighlight: true,
format:'dd/mm/yyyy' 
});
    (function() {
        $(document).ready(function() {
            $('.switch-input').on('change', function() {
                var isChecked = $(this).is(':checked');
                var selectedData;
                var $switchLabel = $('.switch-label');
                console.log('isChecked: ' + isChecked);

                if(isChecked) {
                    $("#type").val("Internal");
                    $("#rpdchange").text('Residence Proof Document');
                    $("#prgn").text('Police Report / Grama Niladari Report (Optional)');
                } else {
                    $("#type").val("External");
                    $("#rpdchange").text('Revenue License');
                    $("#prgn").text('Vehicle Insurance');
                }



            });

            // Params ($selector, boolean)
            function setSwitchState(el, flag) {
                el.attr('checked', flag);
            }

            // Usage
            setSwitchState($('.switch-input'), true);
        });

    })();


</script>
<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
</script>
<script type="text/javascript">
    function parkingFee() {
        if ($('#price').is(':checked')) {
            $('#price-details :input').attr('disabled', true);
        } else {
            $('#price-details :input').removeAttr('disabled');
        }
    }
</script>
<script>
    $('#add-tag').on('click', function (e) {
        if ($('#location-tags').val().trim() !== "") {
            var val = $('#location-tags').val();
            var tagHTML = '<div class="input-group tags-padding">' +
                '<input class="form-control" value="'+val+'" name="tags[]">' +
                '<span class="input-group-addon remove-tag"><i class="glyphicon glyphicon-remove"></i></span>' +
                '</div>';
            $('#show-tags').append(tagHTML);
            $('.remove-tag').on('click', function (e) {
                $(this).parent().remove();
            });
        }
        $('#location-tags').val('');
    });

</script>
<script>


    function removeImage(image,location){
        var id = "#remove-img-"+image;
        swal({
                title: "Are you sure?",
                text: "You want to Delete this?",
                type: "warning",   showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "<?= base_url() ?>owner/deleteLocationImage",
                        type: 'POST',
                        data: {image: image,location:location},
                        success: function (res) {
                            if(res){
                                $(id).hide();
                                $('#show-img').html('');
                                $('#show-img').html(res);
                                swal("Success!", "Successfully Deleted ", "success");
                                $('.dropify').dropify({
                                    error: {
                                        'fileSize': 'The file size is too big ({{ value }} max).',
                                        'minWidth': 'The image width is too small ({{ value }}}px min).',
                                        'maxWidth': 'The image width is too big ({{ value }}}px max).',
                                        'minHeight': 'The image height is too small ({{ value }}}px min).',
                                        'maxHeight': 'The image height is too big ({{ value }}px max).',
                                        'imageFormat': 'The image format is not allowed ({{ value }} only).'
                                    }
                                });
                            }else{

                            }

                        }
                    });
                }
            });
    }

</script>
</body>
</html>