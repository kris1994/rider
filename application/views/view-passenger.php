<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box no-padding">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40 no-padding">
                                    <div class="card-box-padding">
                                     <?php $p = $get_details_passenger; ?>
                                        <div class="row">
                                            <div class="col-4">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <label for="disabledTextInput">First Name</label>
                                                        <p><?php echo (!empty($p->uName) ? $p->uName : '-')?></p>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Last Name</label>
                                                    <p><?php echo '-'?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Email</label>
                                                    <p><?php echo (!empty($p->uEmail) ? $p->uEmail : '-')?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Contact</label>
                                                    <p><?php echo (!empty($p->uPhone) ? $p->uPhone : '-')?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Preferred Rider</label>
                                                    <p><?php echo (!empty($p->uPrefferedRiderType) ? $p->uPrefferedRiderType : '-')?></p>
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label for="TextInput3">Payment Method</label>
                                                    <p><?php echo (!empty($p->uPayment) ? $p->uPayment : '-')?></p>
                                                </fieldset>
                                            </div>

                                        </div>
                                    </div>
                            </div><!-- end col -->
                        </div><!-- end row -->                    
                    </div>
                </div><!-- end col -->

            </div>
            <!-- end row -->
        </div> <!-- container -->
        <div class="container-fluid">
            <div class="col-md-12 col-xs-12 m-t-20">
                <ul class="nav nav-tabs m-b-10" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?php echo (empty($tab_name)) ? 'active' : ''?>" id="home-tab" data-toggle="" href="<?php echo base_url('admin/viewPassenger/'.$uID.'/ride_history')?>" role="tab" aria-controls="home" aria-expanded="true">Ride History</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo ($tab_name == 'planned_rides') ? 'active' : ''?>" id="ride-history-tab" data-toggle="" href="<?php echo base_url('admin/viewPassenger/'.$uID.'/planned_rides')?>"
                           role="tab" aria-controls="profile">Planned Rides</a>
                    </li>
                    <li class="nav-item" style="padding-left: 55%">
                            <a class="nav-link" id="ride-history-tab" data-toggle="" href="javascript:void(0)"
                           role="tab" aria-controls="profile">&nbsp;</a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" id="ride-history-tab" data-toggle="" href="javascript:void(0)"
                           role="tab" aria-controls="profile"><p>Ride Total: <font color="orange" style="font-weight: bold; font-size: 20px"><b>LKR</b> <?php echo $ride_total[0]['fare']; ?></font></p></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card-box no-padding">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12 m-t-sm-40 no-padding">
                                    <div class="card-box-padding">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card-box table-responsive table-margin">
                                                    <table id="datatable" class="table table-striped table-bordered">
                                                        <thead>
                                                        <tr class="div-bottom-line">
                                                            <th>ID</th>
                                                            <th>Rider</th>
                                                            <th>Vehicle Number</th>
                                                            <th>Fare(LKR).</th>
                                                            <th>Distance</th>
                                                            <th>Duration(hrs.min)</th>
                                                        </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php if (count($list_passenger) != 0): ?>
                                                            <?php foreach ($list_passenger as $value): ?>

                                                                <tr>
                                                                    <td><?php echo $value['rID']?></td>
                                                                    <td><?php echo $value['rName']?></td>
                                                                    <td><?php echo $value['vehicle_no']?></td>
                                                                    <td><?php echo $value['fare']?></td>
                                                                    <td><?php echo $value['distance']?></td>
                                                                    <td><?php echo $value['duration']?></td>
                                                               </tr>
                                                             <?php $total_db_rows++; ?>
                                                            <?php endforeach ?>
                                                            <?php
                                                            $detail_show = false;
                                                            ?>
                                                        <?php else: ?>
                                                            <?php
                                                            $detail_show = true;
                                                            ?>
                                                        <?php endif; ?>
                                                        </tbody>
                                                    </table>
                                                    <div class="row">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                            </div><!-- end col -->
                        </div><!-- end row -->                    
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
</div>
<!-- End content-page -->
<script>
    var resizefunc = [];
</script>

</body>
</html>